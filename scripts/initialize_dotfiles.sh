#!/bin/sh

set -e -x

cd

git_df() {
    git --git-dir ~/repos/dotfiles.git "$@"
}

mkdir -p ~/repos ~/.cache
[ -d ~/repos/dotfiles.git ] || git clone --bare https://gitlab.com/kikniknik/dotfiles.git ~/repos/dotfiles.git
[ -f ~/.bashrc ] && mv ~/.bashrc ~/.bashrc.bkp
# .config/fish/config.fish
# .config/fish/fish_variables
git_df config --local status.showUntrackedFiles no
git_df config --local core.bare no
git_df config --local core.worktree "$HOME"
git_df config --local include.path ~/.config/dotfiles/gitconfig
git_df checkout

# essential vim plugins
git_df submodule update --init \
    .vim/pack/mypackages/start/vim-airline \
    .vim/pack/mypackages/start/vim-bbye \
    .vim/pack/mypackages/start/vim-commentary \
    .vim/pack/mypackages/start/vim-fugitive \
    .vim/pack/mypackages/start/vim-gruvbit \
    .vim/pack/mypackages/start/vim-maximizer \
    .vim/pack/mypackages/start/vim-repeat \
    .vim/pack/mypackages/start/vim-slash \
    .vim/pack/mypackages/start/vim-sleuth \
    .vim/pack/mypackages/start/vim-submode \
    .vim/pack/mypackages/start/vim-surround \
    .vim/pack/mypackages/start/vim-unimpaired
