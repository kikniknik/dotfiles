#!/bin/sh

: "${XDG_DATA_HOME:=$HOME/.local/share}"
pkg_file=${1:-$XDG_DATA_HOME/my-programs/arch-packages.txt}

grep -v -e '^#' -e '^\s*$' "$pkg_file" | sudo pacman -S --needed -

# Apply tabbed hack in ueberzug
cat <<EOF | sudo patch /usr/lib/python3.*/site-packages/ueberzug/xutil.py
3a4
> import os
171a173,176
>
>         # tabbed hackj
>         if (os.environ.get('WINDOWID') is not None):
>             pid_window_id_map[os.getpid()] = int(os.environ.get('WINDOWID'))
EOF
