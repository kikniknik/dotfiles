#!/bin/sh

set -e

# Make sure to install paru first
install_paru() {
    temp_dir=$(mktemp -d)
    cd "$temp_dir"
    git clone https://aur.archlinux.org/paru-bin.git
    cd paru-bin
    for f in ./*; do less "$f"; done

    printf "Install paru-bin from AUR (y/n)? "
    read -r yn
    case "$yn" in
	yes|y|Y ) makepkg -sic ;;
	* ) denied=1 ;;
    esac

    cd "$temp_dir"
    rm -rf paru-bin

    if [ "$denied" = 1 ]; then
	echo "Bye."
	return 1
    fi
}

command -v paru >/dev/null || install_paru

: "${XDG_DATA_HOME:=$HOME/.local/share}"
pkg_file=${1:-$XDG_DATA_HOME/my-programs/aur-packages.txt}

grep -v -e '^#' -e '^\s*$' "$pkg_file" | paru -S --needed -
