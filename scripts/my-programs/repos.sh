#!/bin/sh

: "${XDG_DATA_HOME:=$HOME/.local/share}"
repos_file=${1:-$XDG_DATA_HOME/my-programs/repos.txt}
repos_target_dir=${2:-"$HOME/.local/src"}

mkdir -p "$HOME/.local/share/dwm"

mkdir -p "$repos_target_dir"
cd "$repos_target_dir" || exit 1

trap 'rm -f "$CLEANREPOSFILE"' EXIT
CLEANREPOSFILE=$(mktemp)
grep -v -e '^#' -e '^\s*$' "$repos_file" > "$CLEANREPOSFILE"

while IFS=, read -r name url branch; do
    if [ ! -d "$name" ]; then
	git clone "$url" "$name"
	git -C "$name" checkout "$branch"
    fi

    if [ "$name" = "slock" ]; then
	make -C "$name"
	sudo make -C "$name" install
    else
	make -C "$name" PREFIX=~ MANPREFIX=~/.local/share install
    fi
done < "$CLEANREPOSFILE"
