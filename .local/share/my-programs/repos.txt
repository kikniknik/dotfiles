#name,url,branch

# Suckless
dwm,https://gitlab.com/kikniknik/dwm.git,config
dmenu,https://gitlab.com/kikniknik/dmenu.git,config
st,https://gitlab.com/kikniknik/st.git,config
surf,https://gitlab.com/kikniknik/surf.git,config
dwmblocks,https://gitlab.com/kikniknik/dwmblocks.git,config
tabbed,https://gitlab.com/kikniknik/tabbed.git,config

snore,https://github.com/clamiax/snore.git,master
xkblayout-state,https://github.com/nonpop/xkblayout-state.git,master

