" override system lf vim file /usr/share/vim/vimfiles/ftdetect/lf.vim

" command! -nargs=+ LF call lf#LF(<f-args>, [])

function! LF()
    let temp = tempname()
    exec 'silent !lf -selection-path=' . shellescape(temp) fnameescape(expand("%:p"))
    if !filereadable(temp)
        redraw!
        return
    endif
    let names = readfile(temp)
    if empty(names)
        redraw!
        return
    endif
    exec 'edit ' . fnameescape(names[0])
    for name in names[1:]
        exec 'argadd ' . fnameescape(name)
    endfor
    redraw!
endfunction
command! -bar LF call LF()
