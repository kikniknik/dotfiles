" if len(getscriptinfo({"name": "submodes.vim"})) == 0
"   finish
" endif

" win-resize
for key in ['-','<','>']
  call submode#enter_with('win-resize', 'n', '', '<C-W>' . key, '<C-W>' . key)
endfor
call submode#enter_with('win-resize', 'n', '', '<C-W>=', '<C-W>+')

for key in ['_','-','<','>']
  call submode#map('win-resize', 'n', '', key, '<C-W>' . key)
endfor
call submode#map('win-resize', 'n', '', '=', '<C-W>+')
call submode#map('win-resize', 'n', '', '+', '<C-W>=')

" " scroll
" for key in ["<C-D>","<C-F>","<C-B>","<C-U>"]
"   call submode#enter_with('scroll', 'n', '', key, key)
" endfor
" for key in ["d","f","b","u"]
"   call submode#map('scroll', 'n', '', key, '<C-'. toupper(key) .'>')
" endfor

" fold movement
for key in ["zj", "zk"]
  call submode#enter_with('fold-move', 'n', '', key, key)
endfor
for key in ["j", "k"]
  call submode#map('fold-move', 'n', '', key, 'z'.key)
endfor
" for key in ["[z", "]z"]
"   call submode#map('fold-move', 'n', '', key, key)
" endfor
