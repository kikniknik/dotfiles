nnoremap <buffer> gi :let g:netrw_list_hide= netrw_gitignore#Hide()<CR>

" Refresh listing sucks
" nunmap <buffer> <C-L>
nnoremap <buffer> <C-L> <C-W>l
nnoremap <buffer> <C-R> <Plug>NetrwRefresh

" map o to CR>
nnoremap <buffer> o <Plug>NetrwLocalBrowseCheck

" nnoremap <buffer> <expr> <Backspace> b:netrw_curdir == getcwd() ? "<C-W>p:let p = expand('%:h')<CR><C-W>p:execute 'e' p . '/'<CR>" : ":e .<CR>"
