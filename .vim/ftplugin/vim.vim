set tabstop=8
set shiftwidth=2
set softtabstop=2

noremap <buffer> yrr :<C-R><C-L><CR>
vnoremap <buffer> yr "9y:<C-R>9<CR>
