" setlocal scrolloff=99
let b:wants_cursorline = 0
setlocal nocursorline

function s:GoToSection(backwards)
  let flag = "z"

  if a:backwards
    let flag = "b"
    normal k
  endif

  if search('^\(===.*===\|---.*---\)$', "W"..flag)
    normal ztj
  else
    execute "normal" a:backwards ? "gg" : "G"
  endif
endfunction

" nnoremap <buffer> <silent> s j^:call search('\s\{2,\}\*\S\+\*', 'Wz')<CR>^zt
nnoremap <buffer> <silent> ]s j^:call search('\s\{2,\}\*\S\+\*', 'Wz')<CR>^
" nnoremap <buffer> <silent> S :call search('\s\{2,\}\*\S\+\*', 'Wb')<CR>^zt
nnoremap <buffer> <silent> [s :call search('\s\{2,\}\*\S\+\*', 'Wb')<CR>^

nnoremap <buffer> <silent> s :call search('^\s*[A-Z ]\+\s*[~*\n]', 'Wz')<CR>^zt
nnoremap <buffer> <silent> S :call search('^\s*[A-Z ]\+\s*[~*\n]', 'Wbz')<CR>^zt

nnoremap <buffer> <silent> t :call search('[<Bar>`'']\S\+[<Bar>`'']', 'sWz')<CR>
nnoremap <buffer> <silent> T :call search('[<Bar>`'']\S\+[<Bar>`'']', 'sWb')<CR>
nnoremap <buffer> <script> ]] :call <SID>GoToSection(0)<CR>
nnoremap <buffer> <script> <silent> [[ :call <SID>GoToSection(1)<CR>
nnoremap <buffer> <Up> <C-Y>
nnoremap <buffer> <Down> <C-E>
nnoremap <buffer> <nowait> d <C-D>
nnoremap <buffer> <nowait> u <C-U>

nnoremap <buffer> <C-@> :MaximizerToggle<CR>
