let b:ale_linters = [ "ruff", "mypy", "pylsp", "jedils" ]
let b:ale_fixers = [ "ruff", "black" ]

let b:ale_python_pylsp_config = {
\ 'pylsp': {
\   'plugins': {
\     'autopep8': { 'enabled': v:false },
\     'flake8': { 'enabled': v:false },
\     'mccabe': { 'enabled': v:false },
\     'pycodestyle': { 'enabled': v:false },
\     'pydocstyle': { 'enabled': v:false },
\     'pyflakes': { 'enabled': v:false },
\     'pylint': { 'enabled': v:false },
\     'yapf': { 'enabled': v:false },
\     'jedi_completion': { 'enabled': v:false },
\     'jedi_definition': { 'enabled': v:false },
\     'jedi_hover': { 'enabled': v:false },
\     'jedi_references': { 'enabled': v:false },
\     'jedi_signature_help': { 'enabled': v:false },
\     'jedi_symbols': { 'enabled': v:false },
\     'rope_autoimport': { 'enabled': v:true },
\     'rope_completion': { 'enabled': v:true },
\   }
\ }
\}

" let b:ale_python_flake8_options =  "--append-config ~/.config/flake8"

let b:ale_echo_msg_format = "[%severity%] %linter%: %s% (code)%"
let b:ale_loclist_msg_format = "%linter%:% (code)% %s"

setlocal foldmethod=expr
setlocal foldlevel=2

nmap <buffer> K <Plug>(ale_hover)

function! s:GetProjectDirs(fname)
  return map(glob("**/" . a:fname, v:false, v:true), {val -> fnamemodify(v:val, ":p:h")})
endfunction

function! SetPyMakeprg()
  let project_dirs = s:GetProjectDirs("setup.py")
  let project_dirs += s:GetProjectDirs("requirements.txt")
  if len(project_dirs) == 0
    let project_dirs = ["."]
  endif
  let &l:makeprg = "mypy " . join(project_dirs, " ")
endfunction

call SetPyMakeprg()
