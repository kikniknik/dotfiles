" tnoremap <silent> <Leader>T <C-W>p
" nmap <expr> <Leader>T len(<SID>GetTerminalWinnrs()) ? "<Leader>t<Leader>T" : "<Leader>t<C-W>p"

" nnoremap <silent> <Leader>t :call ToggleTerminalFocus(1)<CR>
" tnoremap <silent> <Leader>T <C-W>:call ToggleTerminalFocus(1)<CR>
" nnoremap <silent> <Leader>T :call ToggleTerminalFocus(0)<CR>
" tnoremap <silent> <Leader>t <C-W>:call ToggleTerminalFocus(0)<CR>

function! s:GetTerminalWinnrs()
  return filter(getwininfo(), 'v:val["terminal"]')
endfunction

function! s:SaveBufWinHeight(...)
  if a:0
    call setbufvar(a:1, "wants_height", winheight(bufwinnr(a:1)))
  else
    let b:wants_height = winheight(0)
  endif
endfunction

function! s:RestoreBufWinHeight(...)
  if a:0
    execute bufwinnr(a:1) . "resize" getbufvar(a:1, "wants_height", &termwinsize)
  else
    execute "resize" get(b:, "wants_height", &termwinsize)
  endif
endfunction

function! s:MinimizeTerminal(winnr)
  call s:SaveBufWinHeight(winbufnr(a:winnr))
  execute a:winnr . "resize 1"
endfunction

function! s:GoToTerminalWindow(dir=v:null)
  let winnrs = map(s:GetTerminalWinnrs(), 'v:val["winnr"]')
  if !empty(a:dir)
    let winnrs = filter(winnrs, {idx, val -> getbufvar(winbufnr(val), "my_dir") ==# a:dir})
  endif
  if len(winnrs)
    execute winnrs[0] . "wincmd w"
    return 1
  endif
  return 0
endfunction

function! ToggleTerminalsSize()
  let winnrs = map(s:GetTerminalWinnrs(), 'v:val["winnr"]')
  for nr in winnrs
    if winheight(nr) <= 1
      call s:RestoreBufWinHeight(winbufnr(nr))
    else
      call s:MinimizeTerminal(nr)
    endif
  endfor
endfunction

function! CreateTerminal(dir=v:null)
  execute "botright terminal ++close" $SHELL
  if !empty(a:dir)
    call term_sendkeys(bufnr(), "cd " . shellescape(a:dir) . " && clear\n")
    let b:my_dir = a:dir
  else
    let b:my_dir = getcwd()
  endif
endfunction

function! ToggleTerminalFocus(restore_size=0, dir=v:null)
  if &buftype ==# "terminal"
    if a:restore_size
      call s:MinimizeTerminal(winnr())
    endif
    execute winnr('#') . "wincmd w"
  else
    if s:GoToTerminalWindow(a:dir)
      if a:restore_size
	call s:RestoreBufWinHeight()
      endif
    else
      call CreateTerminal(a:dir)
    endif
  endif
endfunction

function! ToggleTerminal(dir=v:null)
  if &buftype ==# "terminal"
    call HideTerminals()
  else
    if s:GoToTerminalWindow(a:dir)
      if winheight(0) <= 1
	call s:RestoreBufWinHeight()
      endif
    else
      let termbufnrs = term_list()
      if !empty(a:dir)
	let termbufnrs = filter(termbufnrs, {idx, val -> getbufvar(val, "my_dir") ==# a:dir})
      endif
      if len(termbufnrs) > 0
	execute "botright sbuffer" termbufnrs[0]
	call s:RestoreBufWinHeight()
      else
	call CreateTerminal(a:dir)
      endif
    endif
  endif
endfunction

function! ResetTerminalWinSize()
  if &buftype ==# "terminal"
    unlet! b:wants_height
    call <SID>RestoreBufWinHeight()
  else
    for nr in map(s:GetTerminalWinnrs(), 'v:val["winnr"]')
      call setbufvar(winbufnr(nr), "wants_height", &termwinsize)
      call <SID>RestoreBufWinHeight(winbufnr(nr))
    endfor
  endif
endfunction

function! HideTerminals()
  if &buftype ==# "terminal"
    wincmd p
  endif
  let winnrs = map(s:GetTerminalWinnrs(), 'v:val["winnr"]')
  for nr in winnrs
    call s:SaveBufWinHeight(bufwinnr(nr))
    execute nr . "hide"
  endfor
endfunction
