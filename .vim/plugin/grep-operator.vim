nnoremap <Leader>fr :set operatorfunc=<SID>GrepOperator<CR>g@
vnoremap <Leader>fr :<c-u>call <SID>GrepOperator(visualmode())<CR>

function! s:GrepOperator(type)
	let saved_unnamed_register = @@

	if a:type ==# 'v'
		normal! `<v`>y
	elseif a:type ==# 'char'
		normal! `[y`]
	else
		return
	endif

	silent execute "grep! -R " . shellescape(@@) . " ."
	copen
	" execute "FZFRg" @@

	let @@ = saved_unnamed_register
endfunction
