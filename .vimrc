let mapleader = ","
let maplocalleader = "\\"

unlet! skip_defaults_vim
source $VIMRUNTIME/defaults.vim

" cursor shapes
let &t_SI = "\e[6 q"
let &t_SR = "\e[4 q"
let &t_EI = "\e[2 q"

" no focus events
set t_fd=
set t_fe=

" Colorscheme
" You might have to force true color when using regular vim inside tmux as the
" colorscheme can appear to be grayscale with "termguicolors" option enabled.
if !has('gui_running') && &term =~ '^\%(screen\|tmux\)'
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
endif

set termguicolors
silent! colorscheme gruvbit
augroup myvimrc
  autocmd!
  autocmd ColorScheme * highlight! DiffDelete ctermbg=14 gui=undercurl guifg=#a06a47 guisp=#a06a47
augroup END

" Options {{{
" set shell=/bin/sh
set mouse=a
set nottimeout
set number
set relativenumber
set showcmd

" Margin in lines before end/begin
set scrolloff=4
set autoindent

filetype plugin indent on
set tabstop=8
set shiftwidth=4
set softtabstop=4

set pastetoggle=<F2>
noremap Y y$
setlocal foldlevel=6
set foldcolumn=3
set omnifunc=syntaxcomplete#Complete

" Highlight search results and syntax
set hlsearch
set incsearch
set ignorecase smartcase
syntax on
set cursorline

set langmap=ΑA,ΒB,ΨC,ΔD,ΕE,ΦF,ΓG,ΗH,ΙI,ΞJ,ΚK,ΛL,ΜM,ΝN,ΟO,ΠP,QQ,ΡR,ΣS,ΤT,ΘU,ΩV,WW,ΧX,ΥY,ΖZ,αa,βb,ψc,δd,εe,φf,γg,ηh,ιi,ξj,κk,λl,μm,νn,οo,πp,qq,ρr,σs,τt,θu,ωv,ςw,χx,υy,ζz

function! s:SetTabCWD()
  if haslocaldir() != 2
    let buffers = tabpagebuflist()
    if !empty(buffers)
      let git_root = FugitiveWorkTree(buffers[0])
      if !empty(git_root)
        execute "tcd" git_root

      endif
    endif
  endif
endfunction

augroup myvimrc
  " CursorLine in active window
  autocmd BufNew * let b:wants_cursorline = &l:cursorline
  autocmd BufEnter,WinEnter * if get(b:, 'wants_cursorline', 0) | setl cursorline | endif
  autocmd BufLeave,WinLeave * if !exists("b:wants_cursorline") | let b:wants_cursorline = &l:cursorline | endif | setl nocursorline

  " json comments
  autocmd FileType json syntax match Comment +\/\/.\+$+
  autocmd BufNewFile,BufRead *vimspector.json,.gadgets.json,coc-settings.json set filetype=jsonc

  " Disable relativenumber in insert mode
  autocmd WinEnter,FocusGained,InsertLeave * if &number | set relativenumber | endif
  autocmd WinLeave,FocusLost,InsertEnter   * if &number | set norelativenumber | endif

  " Restore cursor position in hidden buffers
  autocmd BufWinEnter * doautocmd vimStartup BufReadPost

  autocmd TabNew * execute "cd" getcwd(-1)
  autocmd TabEnter,BufRead * call <SID>SetTabCWD()

  autocmd FileType help execute "normal \<c-w>H" | vertical resize 84
augroup END
" }}}

if executable("rg")
    set grepprg=rg\ --smart-case\ --vimgrep
    set grepformat=%f:%l:%c:%m,%f:%l:%m
endif

" Get back in window in insert mode
function! s:is_in_insert_mode()
  let mode = mode(1)
  return mode[0] ==# "i" || mode[:1] ==# "ni"
endfunction
augroup myvimrc
  autocmd BufNewFile,BufRead * let b:was_in_insert_mode = 0
  autocmd BufEnter * if exists("b:was_in_insert_mode") && b:was_in_insert_mode | startinsert | elseif <SID>is_in_insert_mode() | stopinsert | endif
  autocmd BufLeave * let b:was_in_insert_mode = <SID>is_in_insert_mode()
augroup END

function s:readonly_scroll(option_old)
  if &readonly
    nnoremap <buffer> <nowait> d <C-D>
    nnoremap <buffer> <nowait> u <C-U>
  elseif a:option_old
    silent! nunmap <buffer> <nowait> d
    silent! nunmap <buffer> <nowait> u
  endif
endfunction

augroup myvimrc
  " Scroll with u, d in readonly buffers
  autocmd BufNew,BufRead call <SID>readonly_scroll(0)
  autocmd OptionSet readonly call <SID>readonly_scroll(v:option_old)
augroup END

" Splits {{{
nnoremap <C-J> <C-W><C-J>
" inoremap <C-J> <C-O><C-W><C-J>
nnoremap <C-K> <C-W><C-K>
" inoremap <C-K> <C-O><C-W><C-K>
nnoremap <C-L> <C-W><C-L>
" inoremap <C-L> <C-O><C-W><C-L>
nnoremap <C-W><C-L> <C-L>
nnoremap <C-H> <C-W><C-H>
" inoremap <C-H> <C-O><C-W><C-H>
set splitbelow
set splitright
nnoremap <C-W>= <C-W>+
nnoremap <C-W>+ <C-W>=
" }}}

" Trailing whitespace
highlight SpecialKey term=bold cterm=bold ctermfg=13 gui=bold guifg=#ff00ff
nnoremap dos :call <SID>TrimWhitespace()<CR>
function! s:TrimWhitespace()
  let save_cursor = getcurpos()
  keepjumps %s/\s\+$//e
  call setpos('.', save_cursor)
endfunction
autocmd myvimrc BufWritePre * call <SID>TrimWhitespace()

" terminal {{{
set termwinsize=18*
tnoremap <C-W><C-W> <C-W>.
tnoremap <C-J> <C-W><C-J>
" tnoremap <C-K> <C-W><C-K>
tnoremap <C-K> <C-W>p
" tnoremap <C-L> <C-W><C-L>
tnoremap <C-H> <C-W><C-H>
tnoremap <C-W>= <C-W>+
tnoremap <C-W>+ <C-W>=
tnoremap `` <C-W>N
tnoremap <C-@> <C-W>:MaximizerToggle<CR>
autocmd myvimrc TerminalOpen * setlocal nobuflisted nonumber norelativenumber foldcolumn=0
nnoremap <silent> <Leader>r :call ToggleTerminalsSize()<CR>
tnoremap <silent> <Leader>r <C-W>:call ToggleTerminalsSize()<CR>
tnoremap <silent> <Leader>R <C-W>:call ResetTerminalWinSize()<CR>
nnoremap <silent> <Leader>R :call ResetTerminalWinSize()<CR>
nnoremap <silent> <Leader>t :call ToggleTerminal(FugitiveWorkTree())<CR>
tnoremap <silent> <Leader>t <C-W>:call ToggleTerminal()<CR>
tnoremap <silent> <Leader>T <C-W>:call ToggleTerminal()<CR>
nnoremap <silent> <Leader>T :call HideTerminals()<CR>
" }}}

" Buffers - Buftabline
set hidden
" nnoremap <C-N> :bnext<CR>
" nnoremap <C-P> :bprev<CR>
nnoremap <expr> <Leader>q &buftype ==# "help" ? ":helpclose<CR>" : ":Bdelete<CR>"
nnoremap <Leader>b :ls<cr>:b<space>
nnoremap <Leader>B :ls<cr>:bdel<space>

let g:CtrlSpaceUseTabline = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_nr_show = 0
let g:airline#extensions#tabline#buffer_idx_mode = 1
let g:airline#extensions#tabline#ctrlspace_show_tab_nr = 1
let g:taboo_tab_format = " %P "

nnoremap <Leader><Leader> <C-^>
inoremap <Leader><Leader> <C-O><C-^>
for i in range(1, 9)
  execute "nmap <Leader>" .. i "<Plug>AirlineSelectTab" .. i
  execute "nmap -"        .. i "<Plug>AirlineSelectTab" .. i
  execute "nmap" i        .. i "<Plug>AirlineSelectTab" .. i
endfor
nmap <Leader>- <Plug>AirlineSelectPrevTab
nmap <Leader>= <Plug>AirlineSelectNextTab
nmap [p <Plug>AirlineSelectPrevTab
nmap ]p <Plug>AirlineSelectNextTab

nmap -- <Leader><Leader>
nmap __ <Leader>q
nmap -0 <Plug>AirlineSelectPrevTab
nmap -= <Plug>AirlineSelectNextTab

nmap 00 <Plug>AirlineSelectPrevTab

cnoremap <C-P> <Up>
cnoremap <C-N> <Down>
cnoremap <C-A> <C-B>
cnoremap <C-B> <C-A>

" wildmenu
set wildmenu
set wildmode=longest:full,full

" recursive find
set path-=/usr/include
set path+=**
set fileignorecase
set wildignorecase
set wildignore+=*.pyc,*.wav
" set suffixes
nnoremap <Leader>f :find<Space>

" Various shorthands
nnoremap <Leader>ev :edit $MYVIMRC<CR>
nnoremap <Leader>el :edit ~/.vim/plugin/local.vim<CR>
nnoremap <Leader>ep :edit ~/.vim/plugin/
nnoremap <Leader>ef :exe "edit ~/.vim/ftplugin/" . &filetype . ".vim"<CR>
nnoremap <Leader>sv :source $MYVIMRC<CR>
nnoremap <Leader>sl :source ~/.vim/plugin/local.vim<CR>
inoremap jk <esc>
" Copy/Paste
noremap <Leader>y "+y
noremap <Leader>yo :redir! > /tmp/vitmp <Bar> silent echon @+ <Bar> redir END<CR>
noremap <Leader>p "+p
noremap <Leader>po :r /tmp/vitmp<CR>
" Save with <C-S>
nnoremap <C-S> :wa<CR>
inoremap <C-S> <Esc>:wa<CR>
" Replace globally with S
nnoremap S <Esc>:%s//g<Left><Left>
nnoremap yoe :set readonly!<CR>
" nnoremap / /\v
nnoremap <Leader>; :<Up>
nnoremap zq :helpclose<CR>
nnoremap <Leader>k :help<Space>
" nnoremap h<Space> :help<Space>
cnoremap <C-H> <C-B>help<Space>
nnoremap <C-X> :helpclose <Bar> NERDTreeClose <Bar> pclose<CR>
nnoremap zk zk[z
nnoremap gw f=w

" Insert mode mappings/abbreviations
inoremap {{{ {<Esc>o}<Esc>O
inoremap ((( (<Esc>o)<Esc>O

" macros on visual: skip not-matching instead of stopping
xnoremap @ :<C-u>call ExecuteMacroOverVisualRange()<CR>
function! ExecuteMacroOverVisualRange()
  echo "@".getcmdline()
  execute ":'<,'>normal @".nr2char(getchar())
endfunction

nnoremap <silent> <Leader>o :call OnlyMain()<CR>
tnoremap <silent> <Leader>o <C-W>:call OnlyMain()<CR>
function! OnlyMain()
  if winnr('$') < 2
    return
  endif

  if win_gettype() || &buftype == "terminal" || index(["help", "nerdtree", "fugitive"], &filetype) > -1
    close
    call OnlyMain()
  else
    only
  endif
endfunction

" quickfix
autocmd myvimrc FileType qf nnoremap <buffer><silent> o <CR>:sleep 400m<CR>:copen<CR>
nnoremap <silent> <Leader>cc :let tempnr = winnr() <Bar> cwindow <Bar> if tempnr == winnr() <Bar> cclose <Bar> endif<CR>

" packages
packadd! matchit

" suckless
function s:make_suckless(make_args)
  if expand("%:t") !~ ".*\.def\.h"
    execute '!sh -c "make -C' expand("%:h") 'PREFIX=~ MANPREFIX=~/.local/share' a:make_args '"'
  endif
endfunction
augroup suckless
  autocmd!
  autocmd BufWritePost ~/.local/src/dwmblocks/*.{c,h} call <SID>make_suckless("install && { pkill dwmblocks; setsid -f dwmblocks; }")
  autocmd BufWritePost ~/.local/src/dwm/*.{c,h} call <SID>make_suckless("install")
  autocmd BufWritePost ~/.local/src/st/*.{c,h} call <SID>make_suckless("clean install")
  autocmd BufWritePost ~/.local/src/dmenu/*.{c,h} call <SID>make_suckless("clean install")
  autocmd BufWritePost ~/.local/src/tabbed/*.{c,h} call <SID>make_suckless("clean install")
  autocmd BufWritePost ~/.config/sxhkd/* silent !pkill -USR1 sxhkd
  autocmd BufWritePost ~/.config/inactivity-lock silent !sh -c 'for i in 11 12; do pkill -RTMIN+$i dwmblocks; done'
augroup END

" fugitive
nnoremap <silent> <C-G>   :call HideTerminals() \| Git<CR>
nnoremap g<Space>         :Git<Space>
nnoremap <Leader>gm       :Git diff master<CR>
nnoremap <Leader>gM       :Git diff master HEAD<CR>
nnoremap <Leader>gd       :Gvdiffsplit<CR>
nnoremap <Leader>gD       :Gvdiffsplit master<CR>
nnoremap <Leader>gb       :Git blame<CR>
nnoremap <Leader>ge       :Gedit master:%
nnoremap <Leader>gw       :Gwrite<CR>
autocmd myvimrc FileType fugitive nmap <buffer><silent> <C-G> :let gnr = winnr() \| wincmd p \| execute gnr . "close"<CR>
autocmd myvimrc FileType fugitive nnoremap <buffer> <C-@> :MaximizerToggle<CR>

" submode
let g:submode_always_show_submode = 1
let g:submode_keep_leaving_key = 1
let g:submode_timeout = 0

" easy align
let g:easy_align_ignore_groups = ['String']
xmap ga <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)

" fzf
let g:fzf_layout = { 'down': '30%' }
" command! -bang -complete=dir -nargs=? LS call fzf#run(fzf#wrap({'source': 'ls', 'dir': <q-args>}, <bang>0))
command! -bang -nargs=? FZFGitFiles call fzf#run(fzf#wrap({'source': 'git ls-files', 'dir': <q-args>}, <bang>0))
command! -bang -nargs=+ FZFRg call fzf#run(fzf#wrap({'source': 'rg --column --line-number --no-heading --color=always --smart-case -- ' . shellescape(<q-args>), 'options': ['--ansi', '--delimiter', ':'], 'sink': function("s:fzf_grep_open") }, <bang>0))
function! s:fzf_grep_open(fn)
  let tokens = split(a:fn, ":")
  execute "edit +" . tokens[1] tokens[0]
endfunction
nnoremap <Leader>ff :execute "FZF" FugitiveWorkTree()<CR>
nnoremap <Leader>fg :execute "FZFGitFiles" FugitiveWorkTree()<CR>

" " ALE
" let g:ale_completion_enabled = 1
" let g:ale_set_signs = 1
" let g:ale_set_highlights = 0
" let g:ale_sign_highlight_linenrs = 1
" let g:ale_sign_column_always = 1
" let g:ale_virtualtext_cursor = 0
" " let g:ale_set_balloons = 1
" let g:ale_floating_preview = 1
" let g:ale_hover_cursor = 0
" " let g:ale_cursor_detail = 1
" " let g:ale_echo_cursor = 0
" let g:airline#extensions#ale#enabled = 1
" nmap <silent> <C-p> <Plug>(ale_previous_wrap)
" nmap <silent> <C-n> <Plug>(ale_next_wrap)
" nnoremap <Leader>ar :ALEFindReferences -quickfix<CR>
" nmap <Leader>af <Plug>(ale_fix)
" nmap <Leader>ad <Plug>(ale_go_to_definition)
" " nmap <Leader>at <Plug>(ale_go_to_type_definition)
" " nmap K <Plug>(ale_hover)
" imap <C-H> <Cmd>ALEHover<CR>
" nmap <Leader>ai <Plug>(ale_import)
" nnoremap <Leader>am :ALERename<CR>
" nnoremap <C-Q> :ALECodeAction<CR>
" nmap <Leader>av <Plug>(ale_detail)
" nnoremap <Leader>a; :ALEInfo<CR>
" nnoremap <Leader>al :ALEPopulateQuickfix<CR>
" nnoremap <Leader>aL :ALEPopulateLocList<CR>

let g:ale_fixers = {
\	'json': [ 'jq' ],
\       'rust': [ 'rustfmt' ]
\ }

" " Use ALE's function for asyncomplete defaults
" au User asyncomplete_setup call asyncomplete#register_source(asyncomplete#sources#ale#get_source_options({
" \ 'priority': 10,
" \ }))

" let g:asyncomplete_log_file = "/tmp/me.log"
imap <c-@> <Plug>(asyncomplete_force_refresh)

" set previewpopup
let g:asyncomplete_auto_completeopt = 0
set completeopt=menuone,noinsert,noselect,popup
" autocmd! CompleteDone * if pumvisible() == 0 | pclose | endif
" let g:python_jedils_use_global = 1

" NERDTree
let NERDTreeWinSize = 50
augroup myvimrc
  " Exit Vim if NERDTree is the only window left.
  " autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif
  " If another buffer tries to replace NERDTree, put it in the other window, and bring back NERDTree.
  " autocmd BufEnter * if bufname('#') =~ 'NERD_tree_\d\+' && bufname('%') !~ 'NERD_tree_\d\+' && winnr('$') > 1 | let buf=bufnr() | buffer# | execute "normal! \<C-W>w" | execute 'buffer'.buf | endif
  " Start NERDTree when Vim is started without file arguments.
  " autocmd StdinReadPre * let s:std_in=1
  " autocmd VimEnter * if argc() == 0 && !exists('s:std_in') | NERDTree | endif
augroup END
nnoremap	<Leader>nn	:execute "NERDTree" FugitiveWorkTree()<CR>
nnoremap	<Leader>nt	:execute "NERDTreeToggle" FugitiveWorkTree()<CR>
nnoremap	<C-_>		:execute "NERDTreeToggle" FugitiveWorkTree()<CR>
tnoremap	<C-_>		<C-W>:execute "NERDTreeToggle" FugitiveWorkTree()<CR>
nnoremap 	<Leader>na	:NERDTreeToggle<CR><C-W>p
nnoremap 	<Leader>nf	:NERDTreeFind<CR>

" netrw
let NERDTreeHijackNetrw = 0
" let loaded_nerd_tree = 1

let g:netrw_liststyle = 3
let g:netrw_winsize = 20
let g:netrw_sizestyle = "H"
" let g:netrw_browse_split = 4
" let g:netrw_chgwin = -1
" nnoremap	<C-_>		:Lexplore<CR>
" tnoremap	<C-_>		<C-W>:Lexplore<CR>

" Maximizer {{{
" let g:maximizer_default_mapping_key = '<F3>'
" let g:maximizer_set_mapping_with_bang = 1
let g:maximizer_set_default_mapping = 0
let g:maximizer_restore_on_winleave = 1
nnoremap <silent> <Space> :MaximizerToggle<CR>
nnoremap <silent> <C-@> :MaximizerToggle<CR>
inoremap <silent> <F3>    <C-O>:MaximizerToggle<CR>
tnoremap <silent> <F3>    <C-W>:MaximizerToggle<CR>
" }}}

" highlightedyank
let g:highlightedyank_highlight_duration = 300

" sendtoterm
map yr  <Plug>(SendToTerm)
nmap yrr <Plug>(SendToTermLine)
nmap yRR ^v$<Plug>(SendToTerm)

" vim_current_word
let g:vim_current_word#enabled = 0
nnoremap yoy :VimCurrentWordToggle<CR>
let g:vim_current_word#highlight_delay = 100
hi CurrentWord ctermbg=LightGrey
hi link CurrentWordTwins CurrentWord

" css-color
nnoremap <script> [oo :call <SID>load_css_color()<CR>
nnoremap ]oo <Nop>
nnoremap <script> yoo :call <SID>load_css_color()<CR>
function! s:load_css_color()
  packadd vim-css-color
  syntax enable
  nnoremap [oo :call css_color#enable()<CR>
  nnoremap ]oo :call css_color#disable()<CR>
  nnoremap yoo :call css_color#toggle()<CR>
endfunction

" vimspector
let g:vimspector_enable_mappings = 'HUMAN'
nmap <Leader>di <Plug>VimspectorBalloonEval
xmap <Leader>di <Plug>VimspectorBalloonEval
nmap <Leader>db <Plug>VimspectorBreakpoints
nnoremap <Leader>dr :VimspectorReset<CR>
nnoremap <Leader>ed :execute "edit ~/.vim/pack/mypackages/start/vimspector/configurations/linux/" . &filetype . "/vimspector.json"<CR>

" gitgutter
nnoremap <Leader>hh :GitGutterToggle<CR>

" conflict-marker
let g:conflict_marker_enable_mappings = 0
" nmap <buffer>]x <Plug>(conflict-marker-next-hunk)
" nmap <buffer>[x <Plug>(conflict-marker-prev-hunk)
nmap cot <Plug>(conflict-marker-themselves)
nmap coo <Plug>(conflict-marker-ourselves)
nmap con <Plug>(conflict-marker-none)
nmap cob <Plug>(conflict-marker-both)
nmap coB <Plug>(conflict-marker-both-rev)

" lf
nnoremap <Leader>l :LF<CR>

" ctrlspace
" let g:CtrlSpaceSaveWorkspaceOnExit = 1
" let g:CtrlSpaceSaveWorkspaceOnSwitch = 1
let g:CtrlSpaceLoadLastWorkspaceOnStart = 1
" augroup myvim_load_workspace
"     autocmd DirChanged global ++nested if !empty(ctrlspace#roots#FindProjectRoot()) | call ctrlspace#workspaces#LoadWorkspace(0, "") | endif
" augroup END
let g:CtrlSpaceStatuslineFunction = "airline#extensions#ctrlspace#statusline()"
" let g:CtrlSpaceProjectRootMarkers = [ ".cs_workspaces" ]
function! CWDTabline()
    let tabline=''
    let tabline.='%#TabLineFill#'
    let tabline.='%=|'      "left/right separator
    let tabline.='%#identifier#'
    let tabline.='%.30(%{fnamemodify(getcwd(), ":~")}%)'
    let tabline.='%*'

    return tabline
endfunction

function! MyTabline()
  return ctrlspace#api#Tabline().CWDTabline()
endfunction

let g:gutentags_ctags_exclude = [ '.mypy_cache' ]

" set tabline=%!MyTabline()
" set showtabline=2
