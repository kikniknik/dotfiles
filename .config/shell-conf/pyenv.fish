# set -gx PYENV_ROOT "$HOME/.local/src/pyenv"
set -gx PYENV_ROOT "$HOME/.pyenv"
contains "$PYENV_ROOT/bin" $PATH; or set -p PATH "$PYENV_ROOT/bin"

if command -v pyenv 1>/dev/null 2>&1
  status --is-interactive; and pyenv init --path | source
  function pyenv
    functions -e pyenv
    pyenv init - | source
    status --is-interactive; and pyenv virtualenv-init - | source
    pyenv $argv
  end
end
