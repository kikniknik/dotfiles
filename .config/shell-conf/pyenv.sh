export PYENV_ROOT="$HOME/repos/_in_use/pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"

if command -v pyenv 1>/dev/null 2>&1; then
	eval "$(pyenv init -)"
	eval "$(pyenv virtualenv-init -)"
else
	>&2 echo "pyenv not found" 
fi
