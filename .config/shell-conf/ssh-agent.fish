# ssh-agent autostart
set -q XDG_CACHE_HOME; or set XDG_CACHE_HOME ~/.cache

if ! command -q ssh-agent
    exit
end

if ! pgrep -u "$USER" ssh-agent > /dev/null
    ssh-agent > $XDG_CACHE_HOME/ssh-agent-thing
end

if [ -z "$SSH_AUTH_SOCK" ] && [ -f $XDG_CACHE_HOME/ssh-agent-thing ]
    while read p
	echo $p | sed -r 's/([A-Z_]+)=([^;]+)/set \1 \2/'
    end < $XDG_CACHE_HOME/ssh-agent-thing | source > /dev/null
end

