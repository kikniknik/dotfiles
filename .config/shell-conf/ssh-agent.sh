# ssh-agent autostart
if ! command -v ssh-agent >/dev/null; then
    return
fi

if ! pgrep -u "$USER" ssh-agent > /dev/null; then
    ssh-agent > "${XDG_CACHE_HOME:=~/.cache}"/ssh-agent-thing
fi

if [ -z "$SSH_AUTH_SOCK" ] && [ -f "$XDG_CACHE_HOME"/ssh-agent-thing ]; then
    eval "$(<"$XDG_CACHE_HOME"/ssh-agent-thing)" > /dev/null
fi
