[alias]
	# shorthands
	c = checkout
	co = commit
	com = commit --amend
	s = status
	d = diff
	b = branch -vva
	p = pull
	pu = push
	puh = push -u origin HEAD
	puf = push --force-with-lease

	l = log --pretty=format:'%C(yellow)%h %Cred%<(16,trunc)%ad %Cblue%<(16,trunc)%an%C(auto)%d %Creset%s' --date=human
	lg = l --graph --all
	t = tag --format '%(color:yellow)%(refname:short)%09%(color:red)%(authordate:short) %(color:brightblack)[%(objecttype)] %(color:reset)%(contents:subject)'
	bb = !/bin/bash -c \"paste -d' ' <(git branch -a --format='%(objectname)' | xargs -i git show -s --format='%C(cyan)%ad%Creset' --date=short --color {}) <(git branch -vav --color)\"
	db = !git fetch --all --prune && git branch --merged master | grep -v '^[ *]*master$' | xargs -r git branch -d
	; db = !git remote prune origin && git branch -vv | awk '/: gone]/ {print $1}' | xargs -r git branch -d
	dc = !"dc() { git rev-list --left-right --count master...$1; }; dc"

[fetch]
	prune = true
[help]
	autoCorrect = -1
[pull]
	ff = only
[tag]
	sort = authordate

[include]
	path = config-local
[merge]
	tool = vimdiff
    conflictstyle = diff3
[status]
	showStash = true

[core]
    pager = delta
[interactive]
    diffFilter = delta --color-only
[pager]
	diff = perl /usr/share/git/diff-highlight/diff-highlight | less
	log = perl /usr/share/git/diff-highlight/diff-highlight | less
	show = perl /usr/share/git/diff-highlight/diff-highlight | less
[delta]
    navigate = true    # use n and N to move between diff sections
    light = false      # set to true if you're in a terminal w/ a light background color (e.g. the default macOS terminal)
[diff]
    colorMoved = default
