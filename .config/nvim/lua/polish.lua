if true then return end -- WARN: REMOVE THIS LINE TO ACTIVATE THIS FILE

-- This will run last in the setup process and is a good place to configure
-- things like custom filetypes. This just pure lua so anything that doesn't
-- fit in the normal config locations above can go here

-- Set up custom filetypes
vim.filetype.add {
  extension = {
    foo = "fooscript",
  },
  filename = {
    ["Foofile"] = "fooscript",
  },
  pattern = {
    ["~/%.config/foo/.*"] = "fooscript",
  },
}

--

vim.api.nvim_create_autocmd("FileType", {
  pattern = "lua,vim",
  callback = function(ev)
    vim.bo.tabstop = 2
    vim.bo.shiftwidth = 2
    vim.bo.keywordprg = ":help"
  end,
})
vim.api.nvim_create_autocmd("FileType", {
  pattern = "yaml,yml,json",
  callback = function(ev)
    vim.bo.tabstop = 2
    vim.bo.shiftwidth = 2
  end,
})

vim.api.nvim_create_autocmd({ "FocusGained", "BufEnter" }, {
  pattern = "*",
  command = "checktime",
})
