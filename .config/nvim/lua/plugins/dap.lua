return {
  {
    "mfussenegger/nvim-dap-python",
    dependencies = { "mfussenegger/nvim-dap" },
    ft = "python",
    config = function()
      local dap = require "dap"
      require("dap-python").setup "~/.local/share/nvim/mason/packages/debugpy/venv/bin/python"
      table.insert(dap.configurations.python, {
        type = "python",
        request = "launch",
        name = "flask-launch",
        python = function() return vim.fn.trim(vim.fn.system "pyenv which python") end,
        module = "flask",
        args = { "run", "--no-reload" },
        env = { TaskManager__WorkersAreThreads = "true" },
        jinja = true,
        -- ... more options, see https://github.com/microsoft/debugpy/wiki/Debug-configuration-settings
      })
      table.insert(dap.configurations.python, {
        type = "python",
        request = "attach",
        name = "Remote docker attach",
        jinja = true,
        connect = {
          host = function() vim.fn.input "Host: " end,
          port = function() vim.fn.input "Port: " end,
        },
        -- ... more options, see https://github.com/microsoft/debugpy/wiki/Debug-configuration-settings
      })
    end,
  },
  {
    "jay-babu/mason-nvim-dap.nvim",
    enabled = false,
    opts = {
      -- ensure_installed = { "stylua", "jq" },
      handlers = {
        function(config)
          -- all sources with no handler get passed here

          -- Keep original functionality
          require("mason-nvim-dap").default_setup(config)
        end,
        cs = function(config)
          config.cs = {
            {
              type = "coreclr",
              name = "attach - netcoredbg",
              request = "attach",
              -- program = function() return vim.fn.input("Path to dll", vim.fn.getcwd() .. "/bin/Debug/", "file") end,
              processId = function()
                local command = 'pgrep -f "^$(git rev-parse --show-toplevel).*/bin/Debug/net*"'
                local handle = io.popen(command)
                local result = handle:read "*a"
                handle:close()
                return result
              end,
              justMyCode = false,
            },
          }
          -- require("mason-nvim-dap").default_setup(config) -- don't forget this!
        end,
      },
    },
  },
  {
    "mfussenegger/nvim-dap",
    config = function()
      local dap = require "dap"
      dap.set_log_level "TRACE"

      function find_proj_root()
        local proj_root = vim.fs.root(0, function(name, path) return name:match "%.csproj$" ~= nil end)
        if proj_root == nil then
          print "Could not find root project directory (containing .csproj)"
          return
        end
      end

      dap.adapters.coreclr = {
        type = "executable",
        command = vim.fn.exepath "netcoredbg",
        args = { "--interpreter=vscode" },
      }
      dap.adapters.coreclr_w = {
        type = "executable",
        command = vim.fn.exepath "netcoredbg",
        args = function()
          local proj_path = vim.fs.find(function(name, path) return name:match ".*Web.*" end, { type = "directory" })[1]
          print(proj_path)
          return { "--interpreter=vscode", "--", "dotnet", "run", "--project", proj_path }
        end,
      }
      dap.configurations.cs = {
        {
          type = "coreclr",
          name = "attach current",
          request = "attach",
          processId = function()
            local command = 'pgrep -f "^$(git rev-parse --show-toplevel).*/bin/Debug/net*"'
            return vim.fn.system(command)
          end,
          justMyCode = false,
        },
        {
          type = "coreclr",
          name = "attach choose",
          request = "attach",
          processId = function()
            dap.utils = require "dap.utils"
            return dap.utils.pick_process { filter = "bin/Debug/net" }
          end,
          justMyCode = false,
        },
        {
          type = "coreclr",
          name = "launch web",
          request = "launch",
          cwd = function()
            local sett_path = vim.fs.find("appsettings.json", {})[1]
            if sett_path ~= nil then
              return vim.fs.dirname(sett_path)
            else
              return vim.fn.getcwd()
            end
          end,
          program = function()
            vim.fn.system "dotnet build"
            local dll_path = vim.fs.find(function(name, path) return name:match ".*Web.*.dll$" end, {})[1]
            -- local proj_path =
            --   vim.fs.find(function(name, path) return name:match ".*Web.*" end, { type = "directory" })[1]
            return dll_path
            -- return vim.fn.input("Path to dll: ", vim.fn.getcwd() .. "/bin/Debug/", "file")
          end,
          args = function()
            local sett_path = vim.fs.find("launchSettings.json", {})[1]
            local url = vim.fn.system("jq -r '.profiles[] | .applicationUrl | select(. != null)' " .. sett_path)
            return { "--urls", url }
          end,
          env = function()
            return {
              ASPNETCORE_ENVIRONMENT = "Development",
              Extensions__Path = vim.fn.getcwd() .. "/Extensions",
            }
          end,
        },
        {
          type = "coreclr",
          name = "launch script",
          request = "launch",
          program = function()
            vim.fn.system "dotnet build"
            local proj_root = vim.fs.root(0, function(name, path) return name:match "%.csproj$" ~= nil end)
            if proj_root == nil then
              print "Could not find root project directory (containing .csproj)"
              return
            end
            return vim.fn.glob(vim.fs.joinpath(proj_root, "bin/Debug/*", vim.fs.basename(proj_root) .. ".dll"))
          end,
        },
      }
    end,
  },
}
