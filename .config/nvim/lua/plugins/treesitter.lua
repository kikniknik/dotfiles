-- if true then return {} end -- WARN: REMOVE THIS LINE TO ACTIVATE THIS FILE

-- Customize Treesitter

---@type LazySpec
return {
  "nvim-treesitter/nvim-treesitter",
  opts = function(_, opts)
    -- add more things to the ensure_installed table protecting against community packs modifying it
    opts.ensure_installed = require("astrocore").list_insert_unique(opts.ensure_installed, {
      "lua",
      "vim",
      -- add more arguments for adding more treesitter parsers
    })
    opts.autotag = {
      enable = true,
      enable_rename = true,
      enable_close = true,
      enable_close_on_slash = true,
      filetypes = { "c_sharp", "cs", "html", "xml" },
    }

    opts.textobjects.move.goto_next_start["]f"] = nil
    opts.textobjects.move.goto_previous_start["[f"] = nil

    opts.textobjects.move.goto_next_start["]m"] = { query = "@function.outer", desc = "Next function start" }
    opts.textobjects.move.goto_next_end["]M"] = { query = "@function.outer", desc = "Next function end" }
    opts.textobjects.move.goto_previous_start["[m"] = { query = "@function.outer", desc = "Previous function start" }
    opts.textobjects.move.goto_previous_end["[M"] = { query = "@function.outer", desc = "Previous function end" }
    opts.textobjects.swap.swap_next[">M"] = { query = "@function.outer", desc = "Swap next function" }
    opts.textobjects.swap.swap_previous["<M"] = { query = "@function.outer", desc = "Swap previous function" }
  end,
}
