return {
  { "tpope/vim-repeat", lazy = false },
  {
    "tpope/vim-fugitive",
    lazy = false,
    config = function(plugin, opts)
      vim.keymap.set("n", "g<Space>", ":Git<Space>")
      vim.keymap.set("n", "<C-g>", function()
        if vim.bo.filetype == "fugitive" or vim.bo.filetype == "floggraph" then
          vim.cmd 'let gnr = winnr() | wincmd p | execute gnr . "close"'
        else
          vim.cmd "Git"
        end
      end)
      vim.keymap.set("n", "g<Space>", ":Git<Space>")
      vim.keymap.set("n", "<Leader>gm", ":Git diff master<CR>")
      vim.keymap.set("n", "<Leader>gM", ":Git diff master HEAD<CR>")
      vim.keymap.set("n", "<Leader>gd", ":Gvdiffsplit<CR>")
      vim.keymap.set("n", "<Leader>gD", ":Gvdiffsplit master<CR>")
      vim.keymap.set("n", "<Leader>gb", ":Git blame<CR>")
      vim.keymap.set("n", "<Leader>ge", ":Gedit master:%")
      vim.keymap.set("n", "<Leader>gw", ":Gwrite<CR>")
      -- vim.keymap.set("n", "<C-g>", ':let gnr = winnr() | wincmd p | execute gnr . "close"<CR>', {buffer = true} )
      -- vim.keymap.set("n", "<C-@>", ':MaximizerToggle<CR>', {buffer = true })

      vim.api.nvim_create_autocmd("FileType", {
        pattern = "fugitive",
        callback = function(args) vim.keymap.set("n", ",t", "<C-g>,t", { buffer = true, remap = true }) end,
      })
    end,
  },
  { "tpope/vim-surround", lazy = false },
  {
    "tpope/vim-commentary",
    lazy = false,
    config = function() vim.cmd "autocmd FileType lua setlocal commentstring=--%s" end,
  },
  { "tpope/vim-unimpaired", lazy = false },
  {
    "kristijanhusak/vim-dadbod-ui",
    dependencies = {
      { "tpope/vim-dadbod", lazy = true },
      { "kristijanhusak/vim-dadbod-completion", ft = { "sql", "mysql", "plsql" }, lazy = true }, -- Optional
    },
    cmd = {
      "DBUI",
      "DBUIToggle",
      "DBUIAddConnection",
      "DBUIFindBuffer",
    },
    init = function() vim.g.db_ui_use_nerd_fonts = 1 end,
  },
  {
    "rbong/vim-flog",
    lazy = false,
    config = function(plugin, opts)
      vim.api.nvim_create_autocmd("FileType", {
        pattern = "fugitive",
        callback = function(args)
          vim.keymap.set("n", "<C-f>", ":close <Bar> botright Flogsplit<CR>", { buffer = true, silent = true })
        end,
      })
      vim.api.nvim_create_autocmd("FileType", {
        pattern = "floggraph",
        callback = function(args)
          vim.cmd [[highlight link flogRef Special]]
          vim.keymap.set("n", "<C-f>", ":close <Bar> Git<CR>", { buffer = true, silent = true })
        end,
      })
    end,
  },
  {
    "rhysd/conflict-marker.vim",
    lazy = false,
    config = function(plugin, opts)
      vim.g.conflict_marker_enable_mappings = 0
      vim.keymap.set("n", "cot", "<Plug>(conflict-marker-themselves)")
      vim.keymap.set("n", "coo", "<Plug>(conflict-marker-ourselves)")
      vim.keymap.set("n", "con", "<Plug>(conflict-marker-none)")
      vim.keymap.set("n", "cob", "<Plug>(conflict-marker-both)")
      vim.keymap.set("n", "coB", "<Plug>(conflict-marker-both-rev)")
    end,
  },
  {
    "szw/vim-maximizer",
    lazy = false,
    config = function()
      vim.g.maximizer_set_default_mapping = 0
      vim.g.maximizer_restore_on_winleave = 1
      vim.keymap.set("n", "<C-@>", "<Cmd>MaximizerToggle<CR>")
      vim.keymap.set("n", "<C-Space>", "<Cmd>MaximizerToggle<CR>")
      vim.keymap.set("t", "<C-@>", "<C-\\><C-O>:MaximizerToggle<CR>")
      vim.keymap.set("t", "<C-Space>", "<C-\\><C-O>:MaximizerToggle<CR>")
    end,
  },
  {
    "habamax/vim-gruvbit",
    lazy = false,
    -- priority = 1000,
    -- config = function()
    --   vim.cmd([[silent! colorscheme gruvbit]])
    -- end,
  },

  { "AndrewRadev/splitjoin.vim", keys = { "gJ", "gS" } },
  { "ellisonleao/glow.nvim", config = true, cmd = "Glow" },
  { "khaveesh/vim-fish-syntax", ft = "fish" },

  {
    "akinsho/toggleterm.nvim",
    keys = ",t",
    opts = function(plugin, opts)
      opts.size = 18
      opts.open_mapping = [[,t]]
      opts.direction = "horizontal"

      vim.api.nvim_exec2(
        [[
          function! ToggleTermMotion(type)
            exec 'normal! `[v`]v'
            ToggleTermSendVisualLines 
          endfunction
        ]],
        { output = false }
      )

      vim.keymap.set("n", "yrr", "<cmd>ToggleTermSendCurrentLine<cr>")
      vim.keymap.set("n", "yr", "<cmd>set opfunc=ToggleTermMotion<cr>g@")
      vim.keymap.set("v", "yr", ":ToggleTermSendVisualSelection<cr>")
    end,
  },

  {
    "sindrets/diffview.nvim",
    lazy = false,
    opts = {
      enhanced_diff_hl = true,
    },
  },

  {
    "Issafalcon/lsp-overloads.nvim",
    event = "VeryLazy",
    -- event = "LspAttach",
    -- config = function(args)
    --   --- Guard against servers without the signatureHelper capability
    --   local client = vim.lsp.get_client_by_id(args.data.client_id)
    --   if client.server_capabilities.signatureHelpProvider then require("lsp-overloads").setup(client, {}) end
    -- end,
  },

  { "cohama/lexima.vim", lazy = false },

  {
    "windwp/nvim-ts-autotag",
    enabled = false,
  },
  {
    "PriceHiller/nvim-ts-autotag",
    -- branch = "fix/close-xml-tags",
    dependencies = "nvim-treesitter/nvim-treesitter",
    -- config = function()
    --   require("nvim-ts-autotag").setup {
    --     -- your config
    --   }
    -- end,
    enabled = false,
    lazy = false,
    -- event = "VeryLazy",
  },
  -- only for non-TreeSitter buffers
  {
    "alvan/vim-closetag",
    lazy = false,
    enabled = true,
    init = function()
      vim.g.closetag_filenames = "*.html,*.xhtml,*.jsx,*.tsx,*.xml,*.cs"
      vim.g.closetag_xhtml_filenames = "*.xhtml,*.jsx,*.tsx,*.cs"
      vim.g.closetag_filetypes = "html,js,xml,cs"
      vim.g.closetag_xhtml_filetype = "xhtml,jsx,tsx,cs"
      vim.g.closetag_emptyTags_caseSensitive = 1
      vim.g.closetag_regions =
        { ["typescript.tsx"] = "jsxRegion,tsxRegion", ["javascript.jsx"] = "jsxRegion", ["cs"] = "csXmlLineComment" }
      vim.g.closetag_shortcut = ">"
    end,
  },

  {
    "L3MON4D3/LuaSnip",
    config = function(plugin, opts)
      require "astronvim.plugins.configs.luasnip"(plugin, opts) -- include the default astronvim config that calls the setup call
      -- add more custom luasnip configuration such as filetype extend or custom snippets
      local luasnip = require "luasnip"
      local s = luasnip.snippet
      local t = luasnip.text_node
      local i = luasnip.insert_node
      luasnip.add_snippets("cs", { s("summary", { t "/// <summary" }) })
    end,
  },

  {
    "hrsh7th/nvim-cmp",
    opts = function(_, opts)
      local cmp = require "cmp"
      opts.mapping["<Tab>"] = function(fallback) fallback() end
      opts.mapping["<S-Tab>"] = function(fallback) fallback() end
      opts.mapping["<C-P>"] = function(fallback)
        if cmp.visible() then
          cmp.select_prev_item()
        else
          fallback()
        end
      end
      opts.mapping["<C-N>"] = function(fallback)
        if cmp.visible() then
          cmp.select_next_item()
        else
          fallback()
        end
      end

      opts.sources = cmp.config.sources {
        { name = "nvim_lsp", priority = 1000 },
        { name = "luasnip", priority = 750 },
        {
          name = "buffer",
          priority = 500,
          option = {
            get_bufnrs = function() return vim.api.nvim_list_bufs() end,
          },
        },
        { name = "path", priority = 250 },
      }
    end,
  },

  {
    "nvim-telescope/telescope.nvim",
    opts = function(_, opts)
      opts.defaults["path_display"] = function(opts, path)
        local tail = require("telescope.utils").path_tail(path)
        return string.format("%s (%s)", tail, path)
      end
    end,
  },

  -- disabled
  { "windwp/nvim-autopairs", enabled = false },
  { "numToStr/Comment.nvim", enabled = false },
  { "max397574/better-escape.nvim", enabled = false },
  { "goolord/alpha-nvim", enabled = false },
}
