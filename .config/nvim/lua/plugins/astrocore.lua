-- if true then return {} end -- WARN: REMOVE THIS LINE TO ACTIVATE THIS FILE

-- AstroCore provides a central place to modify mappings, vim options, autocommands, and more!
-- Configuration documentation can be found with `:h astrocore`
-- NOTE: We highly recommend setting up the Lua Language Server (`:LspInstall lua_ls`)
--       as this provides autocomplete and documentation while editing

---@type LazySpec
return {
  "AstroNvim/astrocore",
  ---@type AstroCoreOpts
  opts = {
    -- Configure core features of AstroNvim
    features = {
      large_buf = { size = 1024 * 500, lines = 10000 }, -- set global limits for large files for disabling features like treesitter
      autopairs = true, -- enable autopairs at start
      cmp = true, -- enable completion at start
      diagnostics_mode = 3, -- diagnostic mode on start (0 = off, 1 = no signs/virtual text, 2 = no virtual text, 3 = on)
      highlighturl = true, -- highlight URLs at start
      notifications = true, -- enable notifications at start
    },
    -- Diagnostics configuration (for vim.diagnostics.config({...})) when diagnostics are on
    diagnostics = {
      virtual_text = true,
      underline = true,
    },
    -- vim options can be configured here
    options = {
      opt = { -- vim.opt.<key>
        relativenumber = true, -- sets vim.opt.relativenumber
        number = true, -- sets vim.opt.number
        spell = false, -- sets vim.opt.spell
        signcolumn = "auto", -- sets vim.opt.signcolumn to auto
        wrap = false, -- sets vim.opt.wrap

        --

        clipboard = "",
        -- completeopt = "menuone,noinsert,noselect",
        wildmode = "longest:full,full",
        tabstop = 4,
        shiftwidth = 4,
        expandtab = false,
        autoread = true,
        -- autoread = false,
        scrolloff = 3,
        grepprg = "rg --vimgrep --no-heading --smart-case",
      },
      g = { -- vim.g.<key>
        -- configure global vim variables (vim.g)
        -- NOTE: `mapleader` and `maplocalleader` must be set in the AstroNvim opts or before `lazy.setup`
        -- This can be found in the `lua/lazy_setup.lua` file
      },
    },
    -- Mappings can be configured through AstroCore as well.
    -- NOTE: keycodes follow the casing in the vimdocs. For example, `<Leader>` must be capitalized
    mappings = {
      -- first key is the mode
      n = {
        -- second key is the lefthand side of the map

        -- navigate buffer tabs with `H` and `L`
        -- L = {
        --   function() require("astrocore.buffer").nav(vim.v.count > 0 and vim.v.count or 1) end,
        --   desc = "Next buffer",
        -- },
        -- H = {
        --   function() require("astrocore.buffer").nav(-(vim.v.count > 0 and vim.v.count or 1)) end,
        --   desc = "Previous buffer",
        -- },

        -- mappings seen under group name "Buffer"
        ["<Leader>bD"] = {
          function()
            require("astroui.status.heirline").buffer_picker(
              function(bufnr) require("astrocore.buffer").close(bufnr) end
            )
          end,
          desc = "Pick to close",
        },
        -- tables with just a `desc` key will be registered with which-key if it's installed
        -- this is useful for naming menus
        ["<Leader>b"] = { desc = "Buffers" },
        -- quick save
        -- ["<C-s>"] = { ":w!<cr>", desc = "Save File" },  -- change description but the same command

        --

        -- ["<Leader>lR"] = {
        --   function() require("telescope.builtin").lsp_references() end,
        --   desc = "Search references",
        --   cond = "textDocument/references",
        -- },

        ["<Leader>e"] = { false, desc = " Edit favorites" },
        ["<Leader>ee"] = {
          function() vim.cmd("edit " .. vim.fs.joinpath(vim.fs.root(0, ".editorconfig"), ".editorconfig")) end,
          desc = ".editorconfig",
        },
        ["<Leader>ec"] = {
          function()
            local token = ""
            if vim.bo.filetype == "cs" or vim.fn.glob "*.sln" ~= "" then
              token = "csproj"
            elseif vim.bo.filetype == "python" then
              token = "toml"
            elseif vim.fn.glob "docker-compose.yml" ~= "" then
              token = "docker-compose.yml"
            end

            require("telescope.builtin").find_files {
              search_file = token,
              on_complete = {
                function(picker)
                  -- remove this on_complete callback
                  picker:clear_completion_callbacks()
                  -- if we have exactly one match, select it
                  if picker.manager.linked_states.size == 1 then
                    require("telescope.actions").select_default(picker.prompt_bufnr)
                  end
                end,
              },
            }
          end,
          desc = "Project conf file",
        },

        --

        -- disable
        ["]e"] = false,
        ["[e"] = false,
        ["[g"] = { "<Plug>(unimpaired-directory-previous)", noremap=false },
        ["]g"] = { "<Plug>(unimpaired-directory-next)", noremap=false },

        [",,"] = { "<C-^>" },
        [",;"] = { ":<up>" },
        ["<C-S>"] = { ":wa<CR>" },
        ["S"] = { "<Esc>:%s//g<Left><Left>" },
        -- ["c"] = { '"_c' },

        ["<Leader>uI"] = { "<Cmd>IBLToggle<CR>", desc = "Toggle indent blankline" },
        ["<Leader>ur"] = { "<Cmd>AstroReload<CR>" },
        ["<Leader>lq"] = { "<Cmd>LspRestart<CR>" },

        -- AstroNvim
        -- ["<C-_>"] = { "<Leader>e", remap = true },
        ["<C-_>"] = {
          function()
            vim.cmd "Neotree toggle"
            if vim.bo.filetype == "neo-tree" then vim.cmd "vertical resize 45" end
          end,
        },
        ["<C-/>"] = { "<C-_>", remap = true },
        [",q"] = {
          function() require("astrocore.buffer").close() end,
          desc = "Close buffer",
        },
        [",1"] = { "[B", remap = true },

        ["<C-H>"] = { ":wincmd h<CR>" },
        ["<C-J>"] = { ":wincmd j<CR>" },
        ["<C-K>"] = {
          function()
            vim.cmd "wincmd k"
            if vim.bo.filetype == "neo-tree" then vim.cmd "wincmd l" end
          end,
        },
        ["<C-L>"] = { ":wincmd l<CR>" },
      },
      i = {
        ["jk"] = { "<esc>" },
        ["<C-S>"] = { "<esc>:wa<CR>" },
        ["{{{"] = { "{<Esc>o}<Esc>O" },
      },
      t = {
        ["``"] = { "<C-\\><C-N>" },
        ["<Esc><Esc>"] = { "<C-\\><C-N>" },
        ["<C-G>"] = { "<Cmd>:q | Git<Cr>" },
        ["<C-J>"] = {
          function() vim.cmd "wincmd j" end,
        },
        ["<C-K>"] = {
          function()
            vim.cmd "wincmd k"
            if vim.bo.filetype == "neo-tree" then vim.cmd "wincmd l" end
          end,
        },
        ["<C-L>"] = {
          function()
            local orig_winid = vim.api.nvim_win_get_number(0)
            vim.cmd "wincmd l"
            local curr_winid = vim.api.nvim_win_get_number(0)
            if orig_winid == curr_winid then vim.api.nvim_chan_send(vim.bo.channel, "clear\n") end
          end,
        },
      },
      v = {},
    },
  },
}
