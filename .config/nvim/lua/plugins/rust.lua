return {
  -- {
  --   "mrcjkb/rustaceanvim",
  --   version = "^5", -- Recommended
  --   lazy = false, -- This plugin is already lazy
  -- },
  {
    "saecki/crates.nvim",
    event = { "BufRead Cargo.toml" },
    dependencies = { "nvim-lua/plenary.nvim" },
    opts = function(plugin, opts)
      opts.completion = {
        cmp = { enabled = true },
      }

      vim.api.nvim_create_autocmd("BufRead", {
        pattern = "Cargo.toml",
        group = vim.api.nvim_create_augroup("CmpSourceCargo", { clear = true }),
        callback = function(args)
          local crates = require "crates"
          function opts(desc) return { silent = true, buffer = true, desc = desc } end

          require("cmp").setup.buffer { sources = { { name = "crates" } } }
          -- require("which-key").register({ ["c"] = { name = "Cargo" } })

          vim.keymap.set("n", "K", crates.show_popup, opts "Cargo popup")
          vim.keymap.set("n", "<C-n>", crates.focus_popup, opts "Cargo focus popup")
          vim.keymap.set("n", "<Leader>c", "<nop>", opts "Cargo")

          vim.keymap.set("n", "<Leader>ct", crates.toggle, opts "toggle")
          vim.keymap.set("n", "<Leader>cr", crates.reload, opts "reload")

          vim.keymap.set("n", "<Leader>cv", crates.show_versions_popup, opts "show_versions_popup")
          vim.keymap.set("n", "<Leader>cf", crates.show_features_popup, opts "show_features_popup")
          vim.keymap.set("n", "<Leader>cd", crates.show_dependencies_popup, opts "show_dependencies_popup")

          vim.keymap.set("n", "<Leader>cu", crates.update_crate, opts "update_crate")
          vim.keymap.set("v", "<Leader>cu", crates.update_crates, opts "update_crates")
          vim.keymap.set("n", "<Leader>ca", crates.update_all_crates, opts "update_all_crates")
          vim.keymap.set("n", "<Leader>cU", crates.upgrade_crate, opts "upgrade_crate")
          vim.keymap.set("v", "<Leader>cU", crates.upgrade_crates, opts "upgrade_crates")
          vim.keymap.set("n", "<Leader>cA", crates.upgrade_all_crates, opts "upgrade_all_crates")

          vim.keymap.set(
            "n",
            "<Leader>ce",
            crates.expand_plain_crate_to_inline_table,
            opts "expand_plain_crate_to_inline_table"
          )
          vim.keymap.set("n", "<Leader>cE", crates.extract_crate_into_table, opts "extract_crate_into_table")

          vim.keymap.set("n", "<Leader>cH", crates.open_homepage, opts "open_homepage")
          vim.keymap.set("n", "<Leader>cR", crates.open_repository, opts "open_repository")
          vim.keymap.set("n", "<Leader>cD", crates.open_documentation, opts "open_documentation")
          vim.keymap.set("n", "<Leader>cC", crates.open_crates_io, opts "open_crates_io")
        end,
      })
    end,
  },
}
