return {
  "nvim-neo-tree/neo-tree.nvim",
  opts = {
    window = {
      mappings = {
        o = { "open", nowait = true },
        oc = "none",
        od = "none",
        og = "none",
        om = "none",
        on = "none",
        os = "none",
        ot = "none",
        Oc = "order_by_created",
        Od = "order_by_diagnostics",
        Og = "order_by_git_status",
        Om = "order_by_modified",
        On = "order_by_name",
        Os = "order_by_size",
        Ot = "order_by_type",
      },
    },
    filesystem = {
      filtered_items = {
        always_show = {
          -- ".gitignore",
          ".env",
          "stuff",
          "conf.yml",
          "conf.yaml",
        },
      },
    },
  },
}
