source ~/.bash_aliases

set -gx XDG_CONFIG_HOME "$HOME/.config"
set -gx XDG_CACHE_HOME "$HOME/.cache"
set -gx XDG_DATA_HOME "$HOME/.local/share"

set -gx DOCKER_CONFIG "$XDG_CONFIG_HOME/docker"
set -gx IPYTHONDIR "$XDG_CONFIG_HOME/ipython"
set -gx MPLAYER_HOME "$XDG_CONFIG_HOME/mplayer"
# conflicts with nvm
# set -gx NPM_CONFIG_USERCONFIG "$XDG_CONFIG_HOME/npm/npmrc"
set -gx R_ENVIRON_USER "$XDG_CONFIG_HOME/R/Renviron"
# set -gx R_PROFILE_USER $HOME/.config/R/Rprofile

set -gx NUGET_PACKAGES "$XDG_CACHE_HOME/NuGetPackages"
set -gx PYLINTHOME "$XDG_CACHE_HOME/pylint"
set -gx LESSHISTFILE "$XDG_CACHE_HOME/less/history"

set -gx CARGO_HOME "$XDG_DATA_HOME/cargo"
set -gx GRADLE_USER_HOME "$XDG_DATA_HOME/gradle"
set -gx GOPATH "$XDG_DATA_HOME/go"
set -gx NVM_DIR $HOME/.local/src/nvm

set -gx EDITOR nvim
set -gx TERMINAL alacritty
set -gx SXHKD_SHELL /bin/sh

# set -gx LESS -W -J -R

contains ~/bin $PATH; or set -p PATH ~/bin
contains ~/.local/bin $PATH; or set -a PATH ~/.local/bin
contains ~/.local/bin/statusbar $PATH; or set -a PATH ~/.local/bin/statusbar
contains ~/.local/bin/more $PATH; or set -a PATH ~/.local/bin/more

# source files
if [ -d $XDG_CONFIG_HOME/shell-conf ]
  for i in $XDG_CONFIG_HOME/shell-conf/*.fish
    if [ -r $i ]
      source $i
    end
  end
  set -e i
end

# abbrs
if status --is-interactive
  # fish config
  abbr -a -g efc $EDITOR ~/.config/fish/config.fish
  abbr -a -g sfc source ~/.config/fish/config.fish

  # git
  abbr -ag g        git
  abbr -ag gits     git status
  abbr -ag gs       git status
  abbr -ag gitc     git checkout
  abbr -ag gc       git checkout
  abbr -ag gitco    git commit
  abbr -ag gco      git commit
  abbr -ag gita     git add
  abbr -ag ga       git add
  abbr -ag gitf     git fetch
  abbr -ag gf       git fetch
  abbr -ag gitr     git remote
  abbr -ag gr       git remote
  abbr -ag gitp     git pull
  abbr -ag gp       git pull
  abbr -ag gitpu    git push
  abbr -ag gpu      git push
  abbr -ag gitpuh   git push -u origin HEAD
  abbr -ag gpuh     git push -u origin HEAD
  abbr -ag gitpuf   git push --force-with-lease
  abbr -ag gpuf     git push --force-with-lease
  abbr -ag gitb     git branch -vva
  abbr -ag gb       git branch -vva
  abbr -ag gitd     git diff
  abbr -ag gd      git diff
  abbr -ag gitdm    git diff master
  abbr -ag gdm      git diff master
  abbr -ag gitdt    git describe --tags
  abbr -ag gdt      git describe --tags
  abbr -ag gitdc    git dc
  abbr -ag gdc      git dc
  abbr -ag gitl     git l -n10
  abbr -ag gl       git l -n10
  abbr -ag gitll    git l -n30
  abbr -ag gll      git l -n30
  abbr -ag gitlg    git lg
  abbr -ag glg      git lg
  abbr -ag gitt     git t
  abbr -ag gt       git t
  abbr -ag gitbb    git bb
  abbr -ag gbb      git bb
  abbr -ag gitdb    git db
  abbr -ag gdb      git db

  abbr -ag ghm gh pr merge -md

  # sudo commands
  abbr -ag sy systemctl
  abbr -ag ssy sudo systemctl
  abbr -ag syu systemctl --user
  abbr -ag sp sudo pacman

  abbr -ag cdls cd ~/.local/src
  abbr -ag els $EDITOR ~/.local/src
  abbr -ag cdlb cd ~/.local/bin
  abbr -ag elb $EDITOR ~/.local/bin
  abbr -ag cdc cd ~/.config
  abbr -ag ec $EDITOR ~/.config

  abbr -ag cdt cd (mktemp -d)
  abbr -ag ll ls -lh
end

# aliases
alias diff="diff --color=auto"
alias cdgr="cd (git rev-parse --show-toplevel)"
alias lf="lfi -last-dir-path=$XDG_CACHE_HOME/lf-last-dir"
alias cdlf="cd (read < $XDG_CACHE_HOME/lf-last-dir)"
alias feh="feh -. --draw-tinted --info ';exiv2 %F 2>/dev/null'"
# alias vg='[ -n "$GIT_DIR" ] && GIT_DIR=$GIT_DIR vim +G || vim +G'

# disable package searches on command not found
function fish_command_not_found
  __fish_default_command_not_found_handler $argv
end

function _set_pwd_based_conf --on-variable PWD --on-event fish_prompt
  status --is-command-substitution; and return

  set -l dotfiles_dir "$HOME/repos/dotfiles.git"

  if [ "$HOME" = "$PWD" ]
    functions -q git || alias git="git --git-dir=$dotfiles_dir"
    alias vg="GIT_DIR=$dotfiles_dir $EDITOR +G"
    # set -g GIT_DIR ~/repos/dotfiles.git
  else if [ "$HOME" = "$dirprev[-1]" ]
    # set -e GIT_DIR
    functions -e git
    alias vg="$EDITOR +G"
  end

  if string match -q $HOME/.local/src\* $PWD
    abbr -q make || abbr -ag make make PREFIX=~ MANPREFIX=~/.local/share
  else if string match -q $HOME/.local/src\* $dirprev[-1]
    abbr -e make
  end

end

[ -f ~/.config/fish/config_more.fish ] && source ~/.config/fish/config_more.fish

# plugins
if functions -q fundle
  fundle plugin 'FabioAntunes/fish-nvm'
  fundle plugin 'edc/bass'
  fundle init
end
