# Defined in /tmp/fish.zq8cOb/suck-conf.fish @ line 2
function suck-conf
	set BASE_REPO_DIR ~/.local/src
	set repo_dir $BASE_REPO_DIR/$argv[1]

	if [ ! -d $repo_dir ]
		echo "No $argv[1] repo in $BASE_REPO_DIR." >&2
		return 1
	end

	# function get_date
	# 	stat --format '%Y' $argv[1]
	# end

	# set mtime (get_date $repo_dir/config.h)
	vim $repo_dir/config.h
	# set new_mtime (get_date $repo_dir/config.h)

	# if [ "$mtime" != "$new_mtime" ]
	# 	if [ "$argv[1]" = st ]
	# 		sudo make -C $repo_dir clean install
	# 	else
	# 		sudo make -C $repo_dir install
	# 	end
	# else
	# 	echo "No changes."
	# end
end
