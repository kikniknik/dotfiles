# Defined in /tmp/fish.qir9YG/timer.fish @ line 2
function timer
  if [ $argv[1] = "-b" ]
    set blocked true
    set -e argv[1]
  end
  set -q argv[1] && set -l dur $argv[1] || set -l dur 5
  set -q argv[2] && set -l msg $argv[2] || set -l msg TIME

  if set -q blocked
    command -v snore >/dev/null && set cmd snore || set cmd sleep
    $cmd $dur
    notify-send $msg
  else
    sleep $dur &
    set job_id (jobs -lg)
    function _notify_$job_id --on-job-exit $job_id --inherit-variable msg
      notify-send $msg
      functions -e _notify_$job_id
    end
  end
end
