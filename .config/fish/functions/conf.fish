function conf
	if [ (count $argv) -eq 0 ]
		if [ -f config.h ] && [ -f config.def.h ]
			vimdiff config.{def.h,h}
		else
			$EDITOR
		end
		return
	end

	switch $argv[1]
		case 'dwm*' st slock ''
			$EDITOR -d ~/.local/src/$argv[1]/config.{def.h,h}
		case ssh
			$EDITOR ~/.ssh/config
		case sshd
			sudo -e /etc/ssh/sshd_config
		case rofi
			$EDITOR ~/.config/rofi/config.rasi
		case sxhkd
			$EDITOR ~/.config/sxhkd/sxhkdrc
		case tmux
			$EDITOR ~/.tmux.conf
		case lf
			$EDITOR ~/.config/lf/lfrc
		case alacritty
			$EDITOR ~/.config/alacritty/alacritty-global.yml
		case '*'
			$EDITOR ~/.config/$argv[1]*
	end

end
