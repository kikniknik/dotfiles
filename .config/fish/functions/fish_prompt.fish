# Defined in /tmp/fish.oyb2ZJ/fish_prompt.fish @ line 2
function fish_prompt
	set last_status $status

	    set -q __fish_git_prompt_showupstream
	    or set -g __fish_git_prompt_showupstream auto

	    function _nim_prompt_wrapper
		set retc $argv[1]
		set field_name $argv[2]
		set field_value $argv[3]

		set_color normal
		set_color $retc
		echo -n '─'
		set_color -o green
		echo -n '['
		set_color normal
		test -n $field_name
		and echo -n $field_name:
		set_color $retc
		echo -n $field_value
		set_color -o green
		echo -n ']'
	    end
	    and set retc green
	    or set retc red

	    set_color $retc
	    echo -n '┬─'
	    set_color -o green
	    echo -n [
	    set_color normal
	    if test "$USER" = root -o "$USER" = toor
		# set_color -o yellow
		set -q $fish_color_user_root; or set fish_color_user_root $fish_color_user
		set_color $fish_color_user_root
	    else
		# set_color normal; set_color cyan
		set_color $fish_color_user
	    end
	    echo -n $USER
	    set_color -o white
	    echo -n @
	    set_color normal
	    if [ -z "$SSH_CLIENT" ]
		# set_color -o blue
		set_color $fish_color_host
	    else
		# set_color -o cyan
		set_color $fish_color_host_ssh
	    end
	    echo -n (prompt_hostname)
	    set_color normal
	    echo -n :
	    # set_color yellow
	    set_color $fish_color_cwd
	    # show full paths (not just first letter)
	    set -g fish_prompt_pwd_dir_length 7
	    echo -n (prompt_pwd)
	    set_color -o green
	    echo -n ']'

	    # Date
	    #_nim_prompt_wrapper $retc '' (date +%X)

	    # Virtual Environment
	    set -q VIRTUAL_ENV
	    and _nim_prompt_wrapper $retc V (basename "$VIRTUAL_ENV")

	    # git
	    [ "$PWD" != "$HOME" ] && set prompt_git (__fish_git_prompt | string trim -c ' ()')
	    test -n "$prompt_git"
	    and _nim_prompt_wrapper $retc G $prompt_git

	    # # Battery status
	    # type -q acpi
	    # and test (acpi -a 2> /dev/null | string match -r off)
	    # and _nim_prompt_wrapper $retc B (acpi -b | cut -d' ' -f 4-)

	    # ls in same line
	    # set_color green; echo -n " --- "; set_color normal; ls -x --color=always | tr "\n" " "

	    # New line
	    echo

	    # # ls below
	    # # set_color normal
	    # set_color -b brblack
	    # set green (tput setaf 2)
	    # set white (tput setaf 7)
	    # set reset_color (tput sgr0)
	    # if [ (count *) -lt 8 ]
		# # ls -x --color=always | sed -e "s/^/$green| $white/"
		# ls -x -F --color=auto --group-directories-first | sed -e "s/^/$green| $white/" | sed -e "s/\s/ /"
	    # end

	    # Background jobs
	    set_color normal
	    for job in (jobs)
		set_color $retc
		echo -n '│ '
		set_color brown
		echo $job
	    end
	    set_color normal
	    set_color $retc
	    echo -n '╰─>'
	    # Prompt
	    if test $USER = "root"; set prompt '#'; else; set prompt '$'; end
	    #set_color -o red
	    if test $last_status -ne 0; set prompt_color 'red'; else; set prompt_color 'normal'; end
	    set_color $prompt_color
	    #echo -n '$ '
	    echo -n "$prompt "
	    set_color normal
end
