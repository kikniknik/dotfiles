# Defined in /tmp/fish.mAmgfl/fish_user_key_bindings.fish @ line 1
function fish_user_key_bindings
	bind \ex startx
	bind / expand-abbr self-insert
	bind \cr editrecentmenu
	bind \cx __fish_prepend_sudo
	# bind \e\[1\;5A history-token-search-backward
	# bind \e\[1\;5B history-token-search-forward

	bind \c] __fish_list_current_token
	bind -k f2 __fish_whatis_current_token
	bind -k f3 __fish_preview_current_file
	bind \c_ edit_command_buffer
	bind \cT __fish_paginate
	bind -k f4 edit_command_executable

  bind \cg "$EDITOR +G"
end
