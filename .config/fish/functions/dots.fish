function dots
	cd
	git-df -c color.status=always status -sb | sed -e '/^## \S\+$/d' -e 's/^/dotfiles:/'
	# git-df rev-list --left-right --count '...@{upstream}' | sed -e '/0\s0/d' -e 's/^/dotfiles:/'
	cd ~/.local/src
	for d in dwm* dmenu st tabbed
		git -C $d -c color.status=always status -sb | sed -e '/^## \S\+$/d' -e 's/^/'$d':/'
		# git -C $d rev-list --left-right --count '...@{upstream}' | sed -e '/0\s0/d' -e 's/^/'$d':/'
	end
	echo
	for d in dwm* dmenu st tabbed
	end
end
