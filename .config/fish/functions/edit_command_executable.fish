function edit_command_executable
	set f (commandline -p | string split -f1 " ")
	if functions -q $f
		funced -s $f
	else if command -q $f
		set f (command -s $f)
		if file -Lb --mime $f | string match -q 'text/*'
			$EDITOR $f
		end
	end
end
