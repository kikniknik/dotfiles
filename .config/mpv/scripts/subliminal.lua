local utils = require 'mp.utils'

tmpdir = "/tmp/mpv-subs"

function load_sub_fn()
    mp.msg.info("Searching subtitle")
    mp.osd_message("Searching subtitle")
    t = {}

    directory, filename = utils.split_path(mp.get_property('path'))
    if directory:find("^http") ~= nil then
      directory = tmpdir
      os.execute("mkdir -p " .. directory)
      mp.commandv("change-list", "sub-file-paths", "add", directory)
    end
    t.args = {"fish", "-c", "subliminal download -l en -l el -d '" .. directory:gsub("'", "'\\''") .. "' '" .. filename:gsub("'", "'\\''") .. "'"}
    res = utils.subprocess(t)
    if res.status == 0 then
        mp.commandv("rescan_external_files", "reselect")
        mp.msg.info("Subtitle download succeeded")
        mp.osd_message("Subtitle download succeeded")
    else
        mp.msg.warn("Subtitle download failed")
        mp.osd_message("Subtitle download failed")
    end
end

mp.add_key_binding("b", "load_subs", load_sub_fn)
