-- Based on https://github.com/wis/mpvSockets/blob/master/mpvSockets.lua

local utils = require 'mp.utils'

tmpdir = "/tmp/mpv-sockets"

os.execute("mkdir -p " .. tmpdir)

pid = utils.getpid()
socketpath = tmpdir  .. "/" .. tostring(pid) .. ".socket"
mp.set_property("options/input-ipc-server", socketpath)

function shutdown_handler()
		os.remove(socketpath)
end
mp.register_event("shutdown", shutdown_handler)
