# The following lines were added by compinstall

#zstyle ':completion:*' completer _expand _complete _ignored _correct _approximate
zstyle ':completion:*' completer _expand _complete _ignored
zstyle :compinstall filename '~/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
# HISTFILE=~/.histfile
HISTFILE=${XDG_DATA_HOME:=~/.local/share}/zsh/history
HISTSIZE=1000
SAVEHIST=1000
setopt extendedglob
bindkey -e
# End of lines configured by zsh-newuser-install

autoload -Uz promptinit
promptinit
prompt adam2

# aliases
alias ls='ls --color=auto'
alias grep='grep --color=auto'

bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# source files
if [ -d ~/shell-conf ]; then
  for i in $(find ~/shell-conf -regex '.*\.z?sh'); do
    if [ -r $i ]; then
      source $i
    fi
  done
  unset i
fi


# source extra stuff
if [ -f ~/.bash_extra ]; then
    . ~/.bash_extra
fi
