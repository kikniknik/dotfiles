local utils = require("astronvim.utils")
local is_available = utils.is_available

local maps = {
  n = {
    [",,"] = { "<C-^>" },
    [",;"] = { ":<up>" },
    ["<C-s>"] = { ":wa<CR>" },
    ["S"] = { "<Esc>:%s//g<Left><Left>" },
    -- ["c"] = { '"_c' },

    ["<leader>uI"] = { "<Cmd>IBLToggle<CR>", desc = "Toggle indent blankline" },
    ["<leader>ur"] = { "<Cmd>AstroReload<CR>" },
    ["<leader>lq"] = { "<Cmd>LspRestart<CR>" },

    -- AstroNvim
    ["<C-_>"] = { "<leader>e", remap = true },
    [",q"] = {
      function()
        require("astronvim.utils.buffer").close()
      end,
      desc = "Close buffer",
    },
    [",1"] = { "[B", remap = true },
  },
  i = {
    ["jk"] = { "<esc>" },
    ["<C-s>"] = { "<esc>:wa<CR>" },
    ["{{{"] = { "{<Esc>o}<Esc>O" },
  },
  t = {
    ["``"] = { "<C-\\><C-N>" },
    ["<Esc><Esc>"] = { "<C-\\><C-N>" },
    ["<C-g>"] = { "<Cmd>:q | Git<Cr>" },
    ["<C-j>"] = {
      function()
        vim.cmd("wincmd j")
      end,
    },
    ["<C-k>"] = {
      function()
        vim.cmd("wincmd k")
        if vim.bo.filetype == "neo-tree" then
          vim.cmd("wincmd l")
        end
      end,
    },
    ["<C-l>"] = {
      function()
        orig_winid = vim.api.nvim_win_get_number(0)
        vim.cmd("wincmd l")
        curr_winid = vim.api.nvim_win_get_number(0)
        if orig_winid == curr_winid then
          vim.api.nvim_chan_send(vim.bo.channel, "clear\n")
        end
      end,
    },
  },
  v = {},
}
maps.n["<C-h>"] = { ":wincmd h<CR>" }
maps.n["<C-j>"] = { ":wincmd j<CR>" }
maps.n["<C-k>"] = maps.t["<C-k>"]
maps.n["<C-l>"] = { ":wincmd l<CR>" }
maps.n["<C-/>"] = maps.n["<C-_>"]

if is_available("vim-fugitive") then
  maps.n["<C-g>"] = {
    function()
      if vim.bo.filetype == "fugitive" or vim.bo.filetype == "floggraph" then
        vim.cmd('let gnr = winnr() | wincmd p | execute gnr . "close"')
      else
        vim.cmd("Git")
      end
    end,
  }
  maps.n["g<Space>"] = { ":Git<Space>" }
  maps.n["<Leader>gm"] = { ":Git diff master<CR>" }
  maps.n["<Leader>gM"] = { ":Git diff master HEAD<CR>" }
  maps.n["<Leader>gd"] = { ":Gvdiffsplit<CR>" }
  maps.n["<Leader>gD"] = { ":Gvdiffsplit master<CR>" }
  maps.n["<Leader>gb"] = { ":Git blame<CR>" }
  maps.n["<Leader>ge"] = { ":Gedit master:%" }
  maps.n["<Leader>gw"] = { ":Gwrite<CR>" }
  -- maps.n["<C-g>"] = { ':let gnr = winnr() | wincmd p | execute gnr . "close"<CR>', buffer = true }
  -- maps.n["<C-@>"] = { ':MaximizerToggle<CR>', buffer = true }

  vim.api.nvim_create_autocmd("FileType", {
    pattern = "fugitive",
    callback = function(args)
      vim.keymap.set("n", ",t", "<C-g>,t", { buffer = true, remap = true })
    end,
  })

  if is_available("vim-flog") then
    vim.api.nvim_create_autocmd("FileType", {
      pattern = "fugitive",
      callback = function(args)
        vim.keymap.set("n", "<C-f>", ":close <Bar> botright Flogsplit<CR>", { buffer = true, silent = true })
      end,
    })
    vim.api.nvim_create_autocmd("FileType", {
      pattern = "floggraph",
      callback = function(args)
        vim.cmd([[highlight link flogRef Special]])
        vim.keymap.set("n", "<C-f>", ":close <Bar> Git<CR>", { buffer = true, silent = true })
      end,
    })
  end
end
if is_available("conflict-marker.vim") then
  vim.g.conflict_marker_enable_mappings = 0
  maps.n["cot"] = { "<Plug>(conflict-marker-themselves)" }
  maps.n["coo"] = { "<Plug>(conflict-marker-ourselves)" }
  maps.n["con"] = { "<Plug>(conflict-marker-none)" }
  maps.n["cob"] = { "<Plug>(conflict-marker-both)" }
  maps.n["coB"] = { "<Plug>(conflict-marker-both-rev)" }
end

if is_available("toggleterm.nvim") then
  vim.api.nvim_exec2(
    [[
    function! ToggleTermMotion(type)
      exec 'normal! `[v`]v'
      ToggleTermSendVisualLines
    endfunction
  ]],
    { output = false }
  )
  maps.n["yrr"] = { "<cmd>ToggleTermSendCurrentLine<cr>" }
  maps.n["yr"] = { "<cmd>set opfunc=ToggleTermMotion<cr>g@" }
  maps.v["yr"] = { ":ToggleTermSendVisualSelection<cr>" }
end

-- if is_available("windows.nvim") then
-- maps.n["<C-@>"] = { "<Cmd>WindowsMaximize<CR>" }
-- maps.t["<C-@>"] = { "<C-\\><C-O>:WindowsMaximize<CR>" }
-- maps.n["<C-Space>"] = maps.n["<C-@>"]
-- maps.t["<C-Space>"] = maps.t["<C-@>"]
-- end
if is_available("vim-maximizer") then
  maps.n["<C-@>"] = { "<Cmd>MaximizerToggle<CR>" }
  maps.t["<C-@>"] = { "<C-\\><C-O>:MaximizerToggle<CR>" }
  maps.n["<C-Space>"] = maps.n["<C-@>"]
  maps.t["<C-Space>"] = maps.t["<C-@>"]
end

if is_available("crates.nvim") then
  vim.api.nvim_create_autocmd("BufRead", {
    pattern = "Cargo.toml",
    group = vim.api.nvim_create_augroup("CmpSourceCargo", { clear = true }),
    callback = function(args)
      local crates = require("crates")
      function opts(desc)
        return { silent = true, buffer = true, desc = desc }
      end

      require("cmp").setup.buffer({ sources = { { name = "crates" } } })
      -- require("which-key").register({ ["c"] = { name = "Cargo" } })

      vim.keymap.set("n", "K", crates.show_popup, opts("Cargo popup"))
      vim.keymap.set("n", "<C-n>", crates.focus_popup, opts("Cargo focus popup"))
      vim.keymap.set("n", "<leader>c", "<nop>", opts("Cargo"))

      vim.keymap.set("n", "<leader>ct", crates.toggle, opts("toggle"))
      vim.keymap.set("n", "<leader>cr", crates.reload, opts("reload"))

      vim.keymap.set("n", "<leader>cv", crates.show_versions_popup, opts("show_versions_popup"))
      vim.keymap.set("n", "<leader>cf", crates.show_features_popup, opts("show_features_popup"))
      vim.keymap.set("n", "<leader>cd", crates.show_dependencies_popup, opts("show_dependencies_popup"))

      vim.keymap.set("n", "<leader>cu", crates.update_crate, opts("update_crate"))
      vim.keymap.set("v", "<leader>cu", crates.update_crates, opts("update_crates"))
      vim.keymap.set("n", "<leader>ca", crates.update_all_crates, opts("update_all_crates"))
      vim.keymap.set("n", "<leader>cU", crates.upgrade_crate, opts("upgrade_crate"))
      vim.keymap.set("v", "<leader>cU", crates.upgrade_crates, opts("upgrade_crates"))
      vim.keymap.set("n", "<leader>cA", crates.upgrade_all_crates, opts("upgrade_all_crates"))

      vim.keymap.set(
        "n",
        "<leader>ce",
        crates.expand_plain_crate_to_inline_table,
        opts("expand_plain_crate_to_inline_table")
      )
      vim.keymap.set("n", "<leader>cE", crates.extract_crate_into_table, opts("extract_crate_into_table"))

      vim.keymap.set("n", "<leader>cH", crates.open_homepage, opts("open_homepage"))
      vim.keymap.set("n", "<leader>cR", crates.open_repository, opts("open_repository"))
      vim.keymap.set("n", "<leader>cD", crates.open_documentation, opts("open_documentation"))
      vim.keymap.set("n", "<leader>cC", crates.open_crates_io, opts("open_crates_io"))
    end,
  })
end

return maps
