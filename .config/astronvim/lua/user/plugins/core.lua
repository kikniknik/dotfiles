return {
  { "tpope/vim-repeat", lazy = false },
  { "tpope/vim-fugitive", lazy = false },
  { "tpope/vim-surround", lazy = false },
  { "tpope/vim-commentary", lazy = false },
  { "tpope/vim-unimpaired", lazy = false },
  { "rbong/vim-flog", lazy = false },
  { "rhysd/conflict-marker.vim", lazy = false },
  {
    "szw/vim-maximizer",
    lazy = false,
    config = function()
      vim.g.maximizer_set_default_mapping = 0
      vim.g.maximizer_restore_on_winleave = 1
    end,
  },
  {
    "habamax/vim-gruvbit",
    lazy = false,
    -- priority = 1000,
    -- config = function()
    --   vim.cmd([[silent! colorscheme gruvbit]])
    -- end,
  },

  { "AndrewRadev/splitjoin.vim", keys = { "gJ", "gS" } },
  { "ellisonleao/glow.nvim", config = true, cmd = "Glow" },
  { "khaveesh/vim-fish-syntax", ft = "fish" },

  {
    "akinsho/toggleterm.nvim",
    keys = ",t",
    opts = {
      size = 18,
      open_mapping = [[,t]],
      direction = "horizontal",
    },
  },

  {
    "sindrets/diffview.nvim",
    lazy = false,
    opts = {
      enhanced_diff_hl = true,
    },
  },

  -- disabled
  { "windwp/nvim-autopairs", enabled = false },
  { "numToStr/Comment.nvim", enabled = false },
  { "max397574/better-escape.nvim", enabled = false },
  { "goolord/alpha-nvim", enabled = false },
}
