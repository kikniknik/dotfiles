return {
  format_on_save = {
    enabled = true, -- enable format on save
    filter = function(bufnr)
      -- if vim.api.nvim_buf_get_name(bufnr) == "CHANGELOG.md" then
      if vim.bo.filetype == "markdown" then
        return false
      end
      return true
    end,
  },
  filter = function(client)
    if vim.fn.expand("%") == "CHANGELOG.md" then
      return false
    end

    -- enable all other clients
    return true
  end,
}
