return {
  n = {
    ["<C-p>"] = { "[d", remap = true },
    ["<C-n>"] = { "]d", remap = true },
    ["<C-q>"] = { "<leader>la", remap = true },
  }
}
