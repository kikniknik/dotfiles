return {
  opt = {
    clipboard = "",
    -- completeopt = "menuone,noinsert,noselect",
    wildmode = "longest:full,full",
    tabstop = 4,
    shiftwidth = 4,
    expandtab = false,
    autoread = true,
    scrolloff = 3,
    grepprg = "rg --vimgrep --no-heading --smart-case",
  },
}
