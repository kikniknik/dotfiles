return function()
  vim.api.nvim_create_autocmd("FileType", {
    pattern = "lua,vim",
    callback = function(ev)
      vim.bo.tabstop = 2
      vim.bo.shiftwidth = 2
      vim.bo.keywordprg = ":help"
    end,
  })
  vim.api.nvim_create_autocmd("FileType", {
    pattern = "yaml,yml,json",
    callback = function(ev)
      vim.bo.tabstop = 2
      vim.bo.shiftwidth = 2
    end,
  })

  vim.api.nvim_create_autocmd({ "FocusGained", "BufEnter" }, {
    pattern = "*",
    command = "checktime",
  })
end
