#!/bin/sh
# shellcheck disable=SC2015

EXIT_CODE=0
[ -f "$LF_TEMP_DIR/minimal_preview" ] && read -r MINIMAL_PREVIEW < "$LF_TEMP_DIR/minimal_preview"

draw_img() {
    if [ -n "$FIFO_UEBERZUG" ]; then
	printf '{"action": "add", "identifier": "preview", "x": %d, "y": %d, "width": %d, "height": %d, "scaler": "contain", "scaling_position_x": 0.5, "scaling_position_y": 0.5, "path": "%s"}\n' "$4" "$5" "$2" "$3" "$1" >"$FIFO_UEBERZUG"
	EXIT_CODE=1
    elif command -v chafa >/dev/null; then
	chafa "$file" -s "$4x"
    else
	return 1
    fi
}

hash() {
    stat --printf '%n\0%i\0%F\0%s\0%W\0%Y' -- "$(readlink -f "$1")" | sha256sum | awk '{print $1}'
}

hash_f() {
    printf '%s/.cache/lf/%s' "$HOME" "$(hash "$1")"
}

try_draw_img() {
    [ "$MINIMAL_PREVIEW" != 1 ] && [ -f "$1" ] && draw_img "$@"
}

file="$1"
height="$3"
shift

page=1
find_page() {
    page_f="$LF_TEMP_DIR/$(hash "$file").page"
    [ -f "$page_f" ] && read -r page < "$page_f"
    if [ -f "$LF_TEMP_DIR/page_diff" ]; then
	read -r page_diff < "$LF_TEMP_DIR/page_diff"
	case "$page_diff" in
	    +1) page=$((page+1)) ;;
	    -1) [ "$page" -gt 1 ] && page=$((page-1)) ;;
	esac
	echo "$page" > "$page_f"
	rm "$LF_TEMP_DIR/page_diff"
    fi
}

print_text() {
    find_page
    s=$((height*(page-1)))
    e=$((s+height))
    [ "$e" != 0 ] || e=""
    # highlight -lm "$s" --line-range "$s-$e" -O ansi "$file" || cat "$file"
    bat --color=always --decorations=never --line-range="$s:$e" "$file" || cat "$file"
}

file_info() {
    file -b "$file"
}

case "$file" in
    *.tgz|*.tar.gz)
	tar tzf "$file" ;;
    *.tar.bz2|*.tbz2)
	tar tjf "$file" ;;
    *.tar.txz|*.txz)
	xz --list "$file" ;;
    *.tar)
	tar tf "$file" ;;
    *.zip|*.jar|*.war|*.ear|*.oxt)
	unzip -l "$file" || file_info ;;
    *.rar)
	unrar l "$file" || file_info ;;
    *.7z)
	7z l "$file" || file_info ;;
    *.torrent)
	[ "$MINIMAL_PREVIEW" != 1 ] && transmission-show "$file" || file_info ;;
    *.iso)
	[ "$MINIMAL_PREVIEW" != 1 ] && iso-info --no-header -l "$file" || file_info ;;
    *.odt|*.ods|*.odp|*.odg|*.odf|*.odb|*.doc|*.docx)
	[ "$MINIMAL_PREVIEW" != 1 ] && soffice --cat "$file" || file_info ;;
    *.xml|*.html)
	[ "$MINIMAL_PREVIEW" != 1 ] && w3m -dump "$file" || print_text ;;
    *.[1-8])
	man "$file" | col -b ;;
    *.pdf|*.ps)
	find_page
	if [ "$MINIMAL_PREVIEW" != 1 ]; then
	    cache_f="$(hash_f "$file")-$page.png"
	    [ -f "$cache_f" ] || gs -o "$cache_f" -sDEVICE=pngalpha -dFirstPage="$page" -dLastPage="$page" "$file" >/dev/null
	fi
	[ "$page" -eq 1 ] && page=0
	try_draw_img "$cache_f" "$@" || pdftotext -f "$page" -l "$page" "$file" - || file_info
	;;
    *.djvu)
	if [ "$MINIMAL_PREVIEW" != 1 ]; then
	    find_page
	    cache_hash="$(hash_f "$file")-$page"
	    if ! [ -f "$cache_hash.png" ]; then
		ddjvu -page="$page" -format=tiff "$file" "$cache_hash.tiff"
		gm convert "$cache_hash.tiff" "$cache_hash.png"
	    fi
	fi
	try_draw_img "$cache_hash.png" "$@" || file_info
	;;
    *.md)
	glow -s dark "$file" || print_text ;;
    *.svg|*.gif)
	if [ "$MINIMAL_PREVIEW" != 1 ]; then
	    find_page
	    cache_f="$(hash_f "$file")-$page.png"
	    [ -f "$cache_f" ] || gm convert "${file}[$page]" "$cache_f"
	fi
	try_draw_img "$cache_f" "$@" || file_info
	;;
    *)
	case $(file -L --mime-type "$file" -b) in
	    text/*|application/csv|inode/x-empty) print_text ;;
	    application/json) jq -C '.' "$file" || print_text ;;
	    message/*) cat "$file" ;;
	    audio/*) mediainfo "$file" ;;
	    image/*)
		if [ "$MINIMAL_PREVIEW" != 1 ]; then
		    cache_f="$file"
		    orientation="$(identify -format '%[EXIF:Orientation]\n' -- "$file")"
		    if [ -n "$orientation" ] && [ "$orientation" != 1 ]; then
			cache_f="$(hash_f "$file").jpg"
			[ -f "$cache_f" ] || convert -- "$file" -auto-orient "$cache_f"
		    fi
		fi
		try_draw_img "$cache_f" "$@" || file_info
	    ;;
	    video/*)
		if [ "$MINIMAL_PREVIEW" != 1 ]; then
		    cache_f="$(hash_f "$file").jpg"
		    [ -f "$cache_f" ] || ffmpeg -y -i "$file" -vframes 1 "$cache_f"
		fi
		try_draw_img "$cache_f" "$@" || mediainfo "$file"
		;;
	    *) file -b "$file" ;;
	esac
esac

# file -Lb -- "$1" | fold -s -w "$width"
exit $EXIT_CODE
