alias git-df="git --git-dir=$HOME/repos/dotfiles.git --work-tree=$HOME"
alias dotfiles='git-df ls-tree --full-tree --name-only -r HEAD'

alias view='vim -R'
alias n='nnn -d'

alias yay='yay --aur'
